package repository

import (
	"gitlab.com/merintisindonesia/membership-backend/model"
	"gorm.io/gorm"
)

type (
	PaymentRepository interface {
		CountingRow() model.PaymentCount
		getByMembershipID(membersId string) (model.Payment, error)
		Create(input model.Payment) (model.Payment, error)
		UpdateById(id string, dataUpdates map[string]interface{}) (model.Payment, error)
		FindByMemebershipId(membershipId string) (model.Payment, error)
	}

	paymentRepository struct {
		db *gorm.DB
	}
)

func NewPaymentRepository(db *gorm.DB) *paymentRepository {
	return &paymentRepository{db: db}
}

func (r *paymentRepository) CountingRow() model.PaymentCount {
	var counting model.PaymentCount

	err := r.db.Raw("SELECT COUNT(*) AS counting FROM payments p").Scan(&counting).Error

	if err != nil {
		return counting
	}

	return counting
}

func (r *paymentRepository) getByMembershipID(membersId string) (model.Payment, error) {
	var payment model.Payment

	if err := r.db.Where("membership_id = ?", membersId).Find(&payment).Error; err != nil {
		return payment, err
	}

	return payment, nil
}

func (r *paymentRepository) Create(input model.Payment) (model.Payment, error) {
	if err := r.db.Create(&input).Error; err != nil {
		return input, err
	}

	return input, nil
}

func (r *paymentRepository) UpdateById(id string, dataUpdates map[string]interface{}) (model.Payment, error) {
	var payment model.Payment

	if err := r.db.Model(&payment).Where("id = ?", id).Updates(dataUpdates).Error; err != nil {
		return payment, err
	}

	if err := r.db.Where("id = ?", id).Find(&payment).Error; err != nil {
		return payment, err
	}

	return payment, nil
}

func (r *paymentRepository) FindByMemebershipId(membershipId string) (model.Payment, error) {
	var payment model.Payment

	if err := r.db.Where("membership_id = ?", membershipId).Find(&payment).Error; err != nil {
		return payment, err
	}

	return payment, nil
}
