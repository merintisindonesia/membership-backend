package repository

import (
	"fmt"

	"gitlab.com/merintisindonesia/membership-backend/model"
	"gitlab.com/merintisindonesia/membership-backend/utils"
	"gorm.io/gorm"
)

type (
	ExamRepository interface {
		GetAll() ([]model.Exam, error)
		GetAllByMemberType(memberTypeID string) ([]model.Exam, error)
		Create(input model.Exam) (model.Exam, error)
		UpdateByID(id string, dataUpdates map[string]interface{}) (model.Exam, error)
		DeleteByID(id string) (utils.DeleteMessageModel, error)
	}

	examRepository struct {
		db *gorm.DB
	}
)

func NewExamRepository(db *gorm.DB) *examRepository {
	return &examRepository{db: db}
}

func (r *examRepository) GetAllByMemberType(memberTypeID string) ([]model.Exam, error) {
	var exams []model.Exam

	if err := r.db.Where("membership_type_id = ?", memberTypeID).Find(&exams).Error; err != nil {
		return exams, err
	}

	return exams, nil
}

func (r *examRepository) GetAll() ([]model.Exam, error) {
	var exams []model.Exam

	if err := r.db.Find(&exams).Error; err != nil {
		return exams, err
	}

	return exams, nil
}

func (r *examRepository) Create(input model.Exam) (model.Exam, error) {
	if err := r.db.Create(&input).Error; err != nil {
		return input, err
	}

	return input, nil
}

func (r *examRepository) UpdateByID(id string, dataUpdates map[string]interface{}) (model.Exam, error) {
	var exam model.Exam

	if err := r.db.Model(&exam).Where("id = ?").Updates(dataUpdates).Error; err != nil {
		return exam, err
	}

	if err := r.db.Where("id = ?", id).Find(&exam).Error; err != nil {
		return exam, err
	}

	return exam, nil
}

func (r *examRepository) DeleteByID(id string) (utils.DeleteMessageModel, error) {
	var message utils.DeleteMessageModel

	if err := r.db.Where("id = ?", id).Delete(&model.Exam{}).Error; err != nil {
		return message, err
	}

	message = *utils.DeleteMessage(fmt.Sprintf("exam id %s success deleted", id), utils.Delete)

	return message, nil
}
