package repository

import (
	"gitlab.com/merintisindonesia/membership-backend/model"
	"gorm.io/gorm"
)

type (
	MembershipExamRepository interface {
		GetAllByMemberID(memberID string) ([]model.MembershipExam, error)
		GetById(id string) (model.MembershipExam, error)
		Create(input model.MembershipExam) (model.MembershipExam, error)
		UpdateByMemberExamID(memberExamID string, dataUpdates map[string]interface{}) (model.MembershipExam, error)
		DeleteById(id string) error
	}

	membershipExamRepository struct {
		db *gorm.DB
	}
)

func NewMembershipExamRepository(db *gorm.DB) *membershipExamRepository {
	return &membershipExamRepository{db: db}
}

func (r *membershipExamRepository) GetAllByMemberID(memberID string) ([]model.MembershipExam, error) {
	var memberExams []model.MembershipExam

	if err := r.db.Find(&memberExams).Error; err != nil {
		return memberExams, err
	}

	return memberExams, nil
}

func (r *membershipExamRepository) GetById(id string) (model.MembershipExam, error) {
	var memberExam model.MembershipExam

	if err := r.db.Where("id = ?", id).Find(&memberExam).Error; err != nil {
		return memberExam, err
	}
	return memberExam, nil
}

func (r *membershipExamRepository) Create(input model.MembershipExam) (model.MembershipExam, error) {
	if err := r.db.Create(&input).Error; err != nil {
		return input, err
	}

	return input, nil
}

func (r *membershipExamRepository) UpdateByMemberExamID(memberExamID string, dataUpdates map[string]interface{}) (model.MembershipExam, error) {
	var memberExam model.MembershipExam

	if err := r.db.Model(&memberExam).Where("id = ?", memberExamID).Updates(dataUpdates).Error; err != nil {
		return memberExam, err
	}

	if err := r.db.Where("id = ?", memberExamID).Find(&memberExam).Error; err != nil {
		return memberExam, err
	}

	return memberExam, nil
}

func (r *membershipExamRepository) DeleteById(id string) error {
	var memberExam model.MembershipExam

	if err := r.db.Where("id = ?", id).Delete(&memberExam).Error; err != nil {
		return err
	}

	return nil
}
