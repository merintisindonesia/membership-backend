package repository

import (
	"gitlab.com/merintisindonesia/membership-backend/model"
	"gorm.io/gorm"
)

type (
	DataMembershipRepository interface {
		GetByUserID(userID string) (model.DataMembership, error)
		GetByID(id string) (model.DataMembership, error)
		Create(input model.DataMembership) (model.DataMembership, error)
		UpdateByID(memberID string, dataUpdates map[string]interface{}) (model.DataMembership, error)
	}

	dataMembershipRepository struct {
		db *gorm.DB
	}
)

func NewDataMembersipRepository(db *gorm.DB) *dataMembershipRepository {
	return &dataMembershipRepository{db: db}
}

func (r *dataMembershipRepository) GetByUserID(userID string) (model.DataMembership, error) {
	var dataMembership model.DataMembership

	if err := r.db.Where("user_id = ? AND deleted = ?", userID, "0").Find(&dataMembership).Error; err != nil {
		return dataMembership, err
	}

	return dataMembership, nil
}

func (r *dataMembershipRepository) Create(input model.DataMembership) (model.DataMembership, error) {
	if err := r.db.Create(&input).Error; err != nil {
		return input, err
	}

	return input, nil
}

func (r *dataMembershipRepository) UpdateByID(memberID string, dataUpdates map[string]interface{}) (model.DataMembership, error) {

	var dataMember model.DataMembership

	if err := r.db.Model(&dataMember).Where("id = ?", memberID).Updates(dataUpdates).Error; err != nil {
		return dataMember, err
	}

	if err := r.db.Where("id = ?", memberID).Find(&dataMember).Error; err != nil {
		return dataMember, err
	}

	return dataMember, nil
}

func (r *dataMembershipRepository) GetByID(id string) (model.DataMembership, error) {
	var dataMember model.DataMembership

	if err := r.db.Raw("SELECT * FROM data_memberships WHERE id = ?", id).Scan(&dataMember).Error; err != nil {
		return dataMember, err
	}

	return dataMember, nil
}
