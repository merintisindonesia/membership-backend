package repository

import (
	"gitlab.com/merintisindonesia/membership-backend/model"
	"gorm.io/gorm"
)

type (
	CertificateRepository interface {
		GetCertificateByMemberID(memberID string)
		Create(input model.Certificate)
	}

	certificateRepository struct {
		db *gorm.DB
	}
)

func NewCertificateRepository() *certificateRepository {
	return &certificateRepository{}
}
