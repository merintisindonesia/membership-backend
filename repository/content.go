package repository

import (
	"gitlab.com/merintisindonesia/membership-backend/model"
	"gorm.io/gorm"
)

type (
	ContentRepository interface {
		GetAll() ([]model.Content, error)
		GetByID(id string) (model.Content, error)

		GetByMemberType(memberTypeID string) ([]model.Content, error)
		GetByMemberTypePremium(idOne, idTwo string) ([]model.Content, error)

		Create(input model.Content) (model.Content, error)
		UpdateByID(id string, dataUpdates map[string]interface{}) (model.Content, error)
	}

	contentRepository struct {
		db *gorm.DB
	}
)

func NewContentRepository(db *gorm.DB) *contentRepository {
	return &contentRepository{db: db}
}

func (r *contentRepository) GetAll() ([]model.Content, error) {
	var contents []model.Content

	if err := r.db.Find(&contents).Error; err != nil {
		return contents, err
	}

	return contents, nil
}
func (r *contentRepository) GetByID(id string) (model.Content, error) {
	var content model.Content

	if err := r.db.Where("id = ?", id).Find(&content).Error; err != nil {
		return content, err
	}

	return content, nil
}
func (r *contentRepository) GetByMemberType(memberTypeID string) ([]model.Content, error) {
	var contents []model.Content

	if err := r.db.Where("membership_type_id = ?", memberTypeID).Find(&contents).Error; err != nil {
		return contents, err
	}

	return contents, nil
}

func (r *contentRepository) GetByMemberTypePremium(idOne, idTwo string) ([]model.Content, error) {

	var contents []model.Content

	if err := r.db.Raw("SELECT * FROM contents WHERE membership_type_id IN (?, ?)", idOne, idTwo).Scan(&contents).Error; err != nil {
		return contents, err
	}

	return contents, nil
}

func (r *contentRepository) Create(input model.Content) (model.Content, error) {
	if err := r.db.Create(&input).Error; err != nil {
		return input, err
	}

	return input, nil
}
func (r *contentRepository) UpdateByID(id string, dataUpdates map[string]interface{}) (model.Content, error) {
	var content model.Content

	if err := r.db.Model(&content).Where("id = ?", id).Updates(dataUpdates).Error; err != nil {
		return content, err
	}

	if err := r.db.Where("id = ?", id).Find(&content).Error; err != nil {
		return content, err
	}

	return content, nil
}
