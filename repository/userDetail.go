package repository

import (
	"gitlab.com/merintisindonesia/membership-backend/model"
	"gorm.io/gorm"
)

type (
	UserDetailRepository interface {
		FindByUserID(id string) (model.UserDetail, error)
		Create(input model.UserDetail) (model.UserDetail, error)
	}

	userDetailRepository struct {
		db *gorm.DB
	}
)

func NewUserDetailRepository(db *gorm.DB) *userDetailRepository {
	return &userDetailRepository{db: db}
}

func (r *userDetailRepository) FindByUserID(id string) (model.UserDetail, error) {
	var userDetail model.UserDetail

	if err := r.db.Where("user_id = ?", id).Find(&userDetail).Error; err != nil {
		return userDetail, err
	}

	return userDetail, nil
}
func (r *userDetailRepository) Create(input model.UserDetail) (model.UserDetail, error) {
	if err := r.db.Create(&input).Error; err != nil {
		return input, err
	}

	return input, nil
}
