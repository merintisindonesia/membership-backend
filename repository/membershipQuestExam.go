package repository

import (
	"gitlab.com/merintisindonesia/membership-backend/model"
	"gorm.io/gorm"
)

type (
	MembershipQuestExamRepo interface {
		GetQuestionByMemberExamId(memberExamId string) ([]model.MembershipQuestionExam, error)
		CreateQuestioExam(input model.MembershipQuestionExam) error
		GetQuestionExamById(id string) (model.MembershipQuestionExam, error)
		UpdateMembershipExamById(id string, dataUpdates map[string]interface{}) (model.MembershipQuestionExam, error)
		DeleteByMemberExamId(memberExamId string) error
	}

	membershipQuestExamrepo struct {
		db *gorm.DB
	}
)

func NewMembershipQuestExamRepository(db *gorm.DB) *membershipQuestExamrepo {
	return &membershipQuestExamrepo{db: db}
}

func (r *membershipQuestExamrepo) GetQuestionByMemberExamId(memberExamId string) ([]model.MembershipQuestionExam, error) {
	var memberQuestExams []model.MembershipQuestionExam

	if err := r.db.Where("membership_exam_id = ?", memberExamId).Find(&memberQuestExams).Error; err != nil {
		return memberQuestExams, err
	}

	return memberQuestExams, nil
}

func (r *membershipQuestExamrepo) CreateQuestioExam(input model.MembershipQuestionExam) error {
	if err := r.db.Create(&input).Error; err != nil {
		return err
	}
	return nil
}

func (r *membershipQuestExamrepo) GetQuestionExamById(id string) (model.MembershipQuestionExam, error) {
	var memberQuestExam model.MembershipQuestionExam

	if err := r.db.Where("id = ?", id).Find(&memberQuestExam).Error; err != nil {
		return memberQuestExam, err
	}

	return memberQuestExam, nil
}
func (r *membershipQuestExamrepo) UpdateMembershipExamById(id string, dataUpdates map[string]interface{}) (model.MembershipQuestionExam, error) {
	var memberQuestExam model.MembershipQuestionExam

	if err := r.db.Model(&memberQuestExam).Where("id = ?", id).Updates(dataUpdates).Error; err != nil {
		return memberQuestExam, err
	}

	memberQuestExamid, err := r.GetQuestionExamById(id)

	if err != nil {
		return memberQuestExamid, err
	}

	return memberQuestExamid, nil
}

func (r *membershipQuestExamrepo) DeleteByMemberExamId(memberExamId string) error {
	var memberQuestExam model.MembershipQuestionExam

	if err := r.db.Where("membership_exam_id = ?", memberExamId).Delete(&memberQuestExam).Error; err != nil {
		return err
	}

	return nil
}
