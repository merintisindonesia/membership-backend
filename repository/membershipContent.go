package repository

import (
	"gitlab.com/merintisindonesia/membership-backend/model"
	"gorm.io/gorm"
)

type (
	MembershipContentRepository interface {
		GetAllByMembershipID(membershipID string) ([]model.MembershipContent, error)
		UpdateByID(id string, dataUpdate map[string]interface{}) (model.MembershipContentWithContent, error)
		Create(input model.MembershipContent) (model.MembershipContent, error)

		GetAllByMembershipIDWIthCOntent(membershipID string) ([]model.MembershipContentWithContent, error)

		GetMembershipContentByID(id string) (model.MembershipContentWithContent, error)
	}

	membershipContentRepository struct {
		db *gorm.DB
	}
)

func NewMembershipContentRepository(db *gorm.DB) *membershipContentRepository {
	return &membershipContentRepository{db: db}
}

func (r *membershipContentRepository) GetAllByMembershipID(membershipID string) ([]model.MembershipContent, error) {
	var membershipContents []model.MembershipContent

	if err := r.db.Where("membership_id = ?", membershipID).Find(&membershipContents).Error; err != nil {
		return membershipContents, err
	}

	return membershipContents, nil
}

func (r *membershipContentRepository) UpdateByID(id string, dataUpdate map[string]interface{}) (model.MembershipContentWithContent, error) {
	var modelMembershipContent model.MembershipContent

	if err := r.db.Model(&modelMembershipContent).Where("id = ?", id).Updates(dataUpdate).Error; err != nil {
		return model.MembershipContentWithContent{}, err
	}

	memberContent, err := r.GetMembershipContentByID(id)
	if err != nil {
		return memberContent, err
	}

	return memberContent, nil
}

func (r *membershipContentRepository) Create(input model.MembershipContent) (model.MembershipContent, error) {
	if err := r.db.Create(&input).Error; err != nil {
		return input, err
	}

	return input, nil
}

func (r *membershipContentRepository) GetAllByMembershipIDWIthCOntent(membershipID string) ([]model.MembershipContentWithContent, error) {

	var memberContents []model.MembershipContentWithContent

	var query = `
SELECT
	mc.id,
	mc.membership_id,
	mc.content_id,
	mc.notes,
	mc.done,
	c.title,
	c.description,
	c.position,
	c.video_url,
	mc.created_at,
	mc.updated_at
FROM membership_contents mc
	LEFT JOIN contents c ON mc.content_id = c.id
where mc.membership_id = ? AND c.deleted = '0'`

	if err := r.db.Raw(query, membershipID).Scan(&memberContents).Error; err != nil {
		return memberContents, err
	}

	return memberContents, nil
}

func (r *membershipContentRepository) GetMembershipContentByID(id string) (model.MembershipContentWithContent, error) {
	var memberContent model.MembershipContentWithContent

	var query = `
SELECT
	mc.id,
	mc.membership_id,
	mc.content_id,
	mc.notes,
	mc.done,
	c.title,
	c.description,
	c.position,
	c.video_url,
	mc.created_at,
	mc.updated_at
FROM membership_contents mc
	LEFT JOIN contents c ON mc.content_id = c.id
where mc.id = ? AND c.deleted = '0'`

	if err := r.db.Raw(query, id).Scan(&memberContent).Error; err != nil {
		return memberContent, err
	}

	return memberContent, nil
}
