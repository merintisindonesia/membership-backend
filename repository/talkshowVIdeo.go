package repository

import (
	"gitlab.com/merintisindonesia/membership-backend/model"
	"gorm.io/gorm"
)

type (
	TalkshowVideoRepository interface {
		GetAll() ([]model.TalkshowVideo, error)
		Create(input model.TalkshowVideo) (model.TalkshowVideo, error)
	}

	talkshowVideoRepository struct {
		db *gorm.DB
	}
)

func NewTalkshowVideoRepository(db *gorm.DB) *talkshowVideoRepository {
	return &talkshowVideoRepository{db: db}
}

func (r *talkshowVideoRepository) GetAll() ([]model.TalkshowVideo, error) {
	var talkshows []model.TalkshowVideo

	if err := r.db.Find(&talkshows).Error; err != nil {
		return talkshows, err
	}

	return talkshows, nil
}

func (r *talkshowVideoRepository) Create(input model.TalkshowVideo) (model.TalkshowVideo, error) {
	if err := r.db.Create(&input).Error; err != nil {
		return input, err
	}

	return input, nil
}
