package repository

import (
	"gitlab.com/merintisindonesia/membership-backend/model"
	"gorm.io/gorm"
)

type (
	MembershipTypeRepository interface {
		GetAll() ([]model.MembershipType, error)
		GetByID(id string) (model.MembershipTypeAll, error)
	}

	membershipTypeRepository struct {
		db *gorm.DB
	}
)

func NewMembershipTypeRepository(db *gorm.DB) *membershipTypeRepository {
	return &membershipTypeRepository{db: db}
}

// get all membership type in database
func (r *membershipTypeRepository) GetAll() ([]model.MembershipType, error) {
	membershipTypes := []model.MembershipType{}

	if err := r.db.Find(&membershipTypes).Error; err != nil {
		return membershipTypes, err
	}

	return membershipTypes, nil
}

func (r *membershipTypeRepository) GetByID(id string) (model.MembershipTypeAll, error) {
	membershipType := model.MembershipTypeAll{}

	if err := r.db.Where("id = ?", id).Preload("Contents").Preload("Exams").Find(&membershipType).Error; err != nil {
		return membershipType, err
	}

	return membershipType, nil
}
