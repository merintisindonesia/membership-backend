package repository

import (
	"gitlab.com/merintisindonesia/membership-backend/model"
	"gorm.io/gorm"
)

type (
	RefferalRepository interface {
		GetByCode(code string) (model.Refferal, error)
		GetAll() ([]model.Refferal, error)
		Create(input model.Refferal) (model.Refferal, error)
		UpdateByCode(code string, dataUpdates map[string]interface{}) (model.Refferal, error)
		UpdateById(id string, dataUpdates map[string]interface{}) (model.Refferal, error)
	}

	refferalRepository struct {
		db *gorm.DB
	}
)

func NewRefferalRepository(db *gorm.DB) *refferalRepository {
	return &refferalRepository{db: db}
}

func (r *refferalRepository) GetByCode(code string) (model.Refferal, error) {
	var refferal model.Refferal

	if err := r.db.Where("code = ?", code).Find(&refferal).Error; err != nil {
		return refferal, err
	}

	return refferal, nil
}

func (r *refferalRepository) GetAll() ([]model.Refferal, error) {
	var refferals []model.Refferal

	if err := r.db.Find(&refferals).Error; err != nil {
		return refferals, err
	}

	return refferals, nil
}

func (r *refferalRepository) Create(input model.Refferal) (model.Refferal, error) {
	if err := r.db.Create(&input).Error; err != nil {
		return input, err
	}

	return input, nil
}

func (r *refferalRepository) UpdateByCode(code string, dataUpdates map[string]interface{}) (model.Refferal, error) {
	var refferal model.Refferal

	if err := r.db.Where("code = ?", code).Model(&refferal).Updates(dataUpdates).Error; err != nil {
		return refferal, err
	}

	if err := r.db.Where("code = ?", code).Find(&refferal).Error; err != nil {
		return refferal, err
	}

	return refferal, nil
}

func (r *refferalRepository) UpdateById(id string, dataUpdates map[string]interface{}) (model.Refferal, error) {
	var refferal model.Refferal

	if err := r.db.Where("id = ?", id).Model(&refferal).Updates(dataUpdates).Error; err != nil {
		return refferal, err
	}

	if err := r.db.Where("id = ?", id).Find(&refferal).Error; err != nil {
		return refferal, err
	}

	return refferal, nil
}
