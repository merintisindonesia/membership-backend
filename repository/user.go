package repository

import (
	"gitlab.com/merintisindonesia/membership-backend/model"
	"gorm.io/gorm"
)

type (
	UserRepository interface {
		Create(user model.User) (model.User, error)
		GetByEmail(email string) (model.User, error)
		GetByID(id string) (model.User, error)
		GetProfileByID(id string) (model.UserMembershipProfile, error)
	}
	userRepository struct {
		db *gorm.DB
	}
)

func NewUserRepository(db *gorm.DB) *userRepository {
	return &userRepository{db: db}
}

func (r *userRepository) GetByEmail(email string) (model.User, error) {
	var user model.User

	if err := r.db.Where("email = ?", email).Find(&user).Error; err != nil {
		return user, err
	}

	return user, nil
}

func (r *userRepository) GetByID(id string) (model.User, error) {
	var user model.User

	if err := r.db.Where("id = ?", id).Find(&user).Error; err != nil {
		return user, err
	}

	return user, nil
}

func (r *userRepository) Create(user model.User) (model.User, error) {
	if err := r.db.Create(&user).Error; err != nil {
		return user, err
	}

	return user, nil
}

func (r *userRepository) GetProfileByID(id string) (model.UserMembershipProfile, error) {
	var query = `
SELECT 
	u.id, 
	u.nm_lengkap, 
	u.email, 
	u.no_hp, 
	dm.type, 
	dm.status, 
	dm.reason_join_member, 
	dm.is_paid, 
	dm.created_at, 
	dm.expired_date, 
	dm.certificate_name, 
	ud.place_birth, 
	ud.date_birth, 
	ud.gender, 
	ud.full_address, 
	ud.agency 
FROM users u
	LEFT JOIN data_memberships dm ON u.id = dm.user_id
	LEFT JOIN user_details ud ON u.id = ud.user_id 
WHERE u.id = ? AND dm.deleted = 0`

	var userMemberProfile model.UserMembershipProfile

	if err := r.db.Raw(query, id).Scan(&userMemberProfile).Error; err != nil {
		return userMemberProfile, err
	}

	return userMemberProfile, nil
}
