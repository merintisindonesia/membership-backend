# FLOW

## FLOW USER

login -> main menu -> dashboard / talkshow -> logout

### FLOW API login (done)

LOGIN
POST email, password TABLE users

VALIDATE 
- email dan password harus diisi
- email dan password harus valid / harus terdaftar di db
- jika berhasil tapi tidak ada membership

GET id users 
FIND TABLE data membership where ID users

- jika tidak ada maka tampilkan "mohon maaf data anda belum terdaftar sebagai membership"
- jika ada tapi is_paid FALSE "pengajuan pembayaran membership anda masih dalam proses konfirmasi"
- jika ada dan is paid TRUE (LANJUT KE NEXT PAGE)

### FLOW API main menu

- no GET API

### FLOW API dashboard

LIST NAVBAR
1. profile

- get table user detail dan get table data membership dari user_id
- show all

- edit user detail table user details

2. Modules

- get all table membership_contents WHERE membership_id = membership_id
- POST done
- POST notes
- get all exams shuffle 20 point question in 50 question
- limit 3 test every weekend / 7 day

3. Certificate

- get table certificates where membership_id = membership_id
- ATOMATIC POST after exam done

4. logout 

### FLOW API GET EXAM
1. start exams

- INSERT INTO membership_exams WITH value score zero (time limit) (60 menit)
- jika selesai langsung otomatis di cek ke database dengan cara menghitung otomatis di front end kemudian di database langsung input score
a. jika score diatas atau sama dengan 75 maka generate certificate
b. jika dibawah 75 maka langsung harus coba lagi

### FLOW API talkshow

- list all video talkshow in TABLE talkshow_videos

## FLOW DEVELOPER

1. Login developer

env (middleware developer and admin)

email : developer.membership@merintisindonesia.com
password : developermerintisindonesia1234

set token
role=developer

1. add membershipType fixed 4 data 

INSERT table membershipType  

- perintis - basic
- perintis - premium
- pengembang - basic
- pengembang - premium

2. add video membership (done)

INSERT table content
- video with parameter membershipType

3. add exams 
INSERT table exams


## BY SYSTEM

1. certificate
- create if exam


 got 75 with name and other, pakai REST API POST certifiacet

2. exam
- create if user take exam in front-end



## v2 payment in subdomain membership 

1. user can create data membership (validate if user registered)
2. user can upload payment if has been paid membership
3. system kan send email if admin accept the payment proof
- email success paid
- email welcoming to membership


