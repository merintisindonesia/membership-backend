-- sql syntax database



-- database system get membership_contents with contents
-- EXAMPLE : 
SELECT
	mc.id,
	mc.membership_id,
	mc.content_id,
	mc.notes,
	mc.done,
	c.title,
	c.description,
	c.position,
	mc.created_at,
	mc.updated_at
FROM membership_contents mc
	LEFT JOIN contents c ON mc.content_id = c.id
where mc.membership_id = '8825ba6e-7a22-4e1b-adf2-ad0fad815e41'

-- sql to get profile data membership
-- process migration data
SELECT 
	u.id, 
	u.nm_lengkap, 
	u.email, 
	u.no_hp, 
	dm.type, 
	dm.status, 
	dm.reason_join_member, 
	dm.is_paid, 
	dm.created_at, 
	dm.expired_date, 
	dm.certificate_name, 
	ud.place_birth, 
	ud.date_birth, 
	ud.gender, 
	ud.full_address, 
	ud.agency 
FROM users u
	LEFT JOIN data_memberships dm ON u.id = dm.user_id
	LEFT JOIN user_details ud ON u.id = ud.user_id 
WHERE u.id = 60 AND dm.deleted = 0