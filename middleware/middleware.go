package middleware

import (
	"errors"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"gitlab.com/merintisindonesia/membership-backend/auth"
	"gitlab.com/merintisindonesia/membership-backend/utils"
)

func Middleware(authService auth.AuthService) gin.HandlerFunc {
	return func(c *gin.Context) {
		authHeader := c.GetHeader("Authorization")

		if authHeader == "" || len(authHeader) == 0 {
			c.AbortWithStatusJSON(401, utils.ErrorMessages(utils.ErrorUnauthorizeUser, errors.New("error user unauthorize")))
			return
		}

		token, err := authService.ValidateToken(authHeader)

		if err != nil {
			c.AbortWithStatusJSON(401, utils.ErrorMessages(utils.ErrorUnauthorizeUser, errors.New("error user unauthorize")))
			return
		}

		claim, ok := token.Claims.(jwt.MapClaims)

		if !ok {
			c.AbortWithStatusJSON(401, utils.ErrorMessages(utils.ErrorUnauthorizeUser, errors.New("error user unauthorize")))
			return
		}

		userID := int(claim["user_id"].(float64))
		memberID := claim["membership_id"].(string)
		memberType := claim["membership_type"].(string)
		memberStatus := claim["membership_status"].(string)
		role := claim["role"].(string)

		// set data di handler
		c.Set("user_id", userID)
		c.Set("membership_id", memberID)
		c.Set("membership_type", memberType)
		c.Set("membership_status", memberStatus)
		c.Set("role", role)
	}
}

func AdminDevMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		val, ok := c.Get("role")

		if !ok {
			c.AbortWithStatusJSON(401, utils.ErrorMessages(utils.ErrorUnauthorizeUser, errors.New("error access only for admin / developer")))
			return
		}

		if val.(string) == "admin" || val.(string) == "developer" {
			c.Next()
			return
		}

		c.AbortWithStatusJSON(401, utils.ErrorMessages(utils.ErrorUnauthorizeUser, errors.New("error access only for admin / developer")))
	}
}
