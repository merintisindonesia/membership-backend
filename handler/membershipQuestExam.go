package handler

import (
	"errors"

	"github.com/gin-gonic/gin"
	"gitlab.com/merintisindonesia/membership-backend/model"
	"gitlab.com/merintisindonesia/membership-backend/service"
	"gitlab.com/merintisindonesia/membership-backend/utils"
)

type (
	memberQuestExamHandler struct {
		memberQuestExamService service.MemberQuestExamService
	}
)

func NewMemberQuestExamHandler(memberQuestExamService service.MemberQuestExamService) *memberQuestExamHandler {
	return &memberQuestExamHandler{memberQuestExamService: memberQuestExamService}
}

func (h *memberQuestExamHandler) GetAllExamByMemberExamId(c *gin.Context) {
	var memberExamid = c.Params.ByName("membership_exam_id")

	if memberExamid == "" {
		c.JSON(400, utils.ErrorMessages(utils.ErrorParameterNotFound, errors.New("please insert membership exam id parameter")))
		return
	}

	membershipQuestExams, err := h.memberQuestExamService.GetAllByMemberExamId(memberExamid)

	if err != nil {
		c.JSON(500, utils.ErrorMessages(utils.ErrorInternalServer, err))
		return
	}

	c.JSON(200, membershipQuestExams)
}

func (h *memberQuestExamHandler) UpdateAnswerQuestionExam(c *gin.Context) {
	var questionExamId = c.Params.ByName("membership_question_exam_id")
	var answerInput model.MembershipQuestExamInputAnswer

	if questionExamId == "" {
		c.JSON(400, utils.ErrorMessages(utils.ErrorParameterNotFound, errors.New("please insert membership exam id parameter")))
		return
	}

	if err := c.ShouldBindJSON(&answerInput); err != nil {
		c.JSON(400, utils.ErrorMessages(utils.ErrorBadRequest, err))
		return
	}

	memberQuestExam, err := h.memberQuestExamService.UpdateAnswerByQuestionExamId(questionExamId, answerInput)

	if err != nil {
		c.JSON(500, utils.ErrorMessages(utils.ErrorInternalServer, err))
		return
	}

	c.JSON(200, memberQuestExam)
}
