package handler

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/merintisindonesia/membership-backend/model"
	"gitlab.com/merintisindonesia/membership-backend/service"
	"gitlab.com/merintisindonesia/membership-backend/utils"
)

type (
	dataMembershipHandler struct {
		dataMembershipService service.DataMembershipService
	}
)

func MewDataMembershipHandler(dataMembershipService service.DataMembershipService) *dataMembershipHandler {
	return &dataMembershipHandler{dataMembershipService: dataMembershipService}
}

/*
data membership/login

*/

func (h *dataMembershipHandler) LoginMembership(c *gin.Context) {
	var userLoginInput model.UserLoginInput

	if err := c.ShouldBindJSON(&userLoginInput); err != nil {
		c.JSON(400, utils.ErrorMessages(utils.ErrorBadRequest, err))
		return
	}

	dataMembershipLogin, err := h.dataMembershipService.LoginWithUserAccount(userLoginInput)

	if err != nil {
		c.JSON(500, utils.ErrorMessages(utils.ErrorInternalServer, err))
		return
	}

	c.JSON(200, dataMembershipLogin)
}

func (h *dataMembershipHandler) CreateMembershipUser(c *gin.Context) {
	var input model.DataMembershipRegisterInput

	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(400, utils.ErrorMessages(utils.ErrorBadRequest, err))
		return
	}

	dataMembership, err := h.dataMembershipService.CreateUserMembership(input)

	if err != nil {
		c.JSON(500, utils.ErrorMessages(utils.ErrorInternalServer, err))
		return
	}

	c.JSON(201, dataMembership)
}

func (h *dataMembershipHandler) UpdatePaidAndBulkContentMember(c *gin.Context) {
	memberId := c.Params.ByName("membership_id")

	paidMessage, err := h.dataMembershipService.PaidMemberAndBulkContent(memberId)

	if err != nil {
		c.JSON(500, utils.ErrorMessages(utils.ErrorInternalServer, err))
		return
	}

	c.JSON(200, paidMessage)
}

func (h *dataMembershipHandler) GetProfileMembership(c *gin.Context) {
	memberID, _ := c.Get("membership_id")
	userID, _ := c.Get("user_id")

	profileMembership, err := h.dataMembershipService.GetMembershipProfile(memberID.(string), int(userID.(int)))

	if err != nil {
		c.JSON(500, utils.ErrorMessages(utils.ErrorInternalServer, err))
		return
	}

	c.JSON(200, profileMembership)

}

func (h *dataMembershipHandler) GetNotificationPaymentMembership(c *gin.Context) {
	var input model.PaymentNotificationInput

	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(400, utils.ErrorMessages(utils.ErrorBadRequest, err))
		return
	}

	notification, err := h.dataMembershipService.ProcessPaymentWithMidtrans(input)

	if err != nil {
		c.JSON(500, utils.ErrorMessages(utils.ErrorInternalServer, err))
		return
	}

	c.JSON(200, notification)
}
