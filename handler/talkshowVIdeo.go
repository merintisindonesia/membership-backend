package handler

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/merintisindonesia/membership-backend/model"
	"gitlab.com/merintisindonesia/membership-backend/service"
	"gitlab.com/merintisindonesia/membership-backend/utils"
)

type (
	talkshowVideoHandler struct {
		talkshowService service.TalkShowVideoService
	}
)

func NewTalkshowVideoHandler(talkshowService service.TalkShowVideoService) *talkshowVideoHandler {
	return &talkshowVideoHandler{talkshowService: talkshowService}
}

func (h *talkshowVideoHandler) GetAllTalkshow(c *gin.Context) {
	talkshows, err := h.talkshowService.GetAll()

	if err != nil {
		c.JSON(500, utils.ErrorMessages(utils.ErrorInternalServer, err))
		return
	}

	c.JSON(200, talkshows)
}

func (h *talkshowVideoHandler) CreateNewTalkshow(c *gin.Context) {

	var input model.TalkshowVideoInput

	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(400, utils.ErrorMessages(utils.ErrorBadRequest, err))
		return
	}

	talkshow, err := h.talkshowService.CreateNewTalkshow(input)

	if err != nil {
		c.JSON(500, utils.ErrorMessages(utils.ErrorInternalServer, err))
		return
	}

	c.JSON(201, talkshow)
}

func (h *talkshowVideoHandler) BulkCreateTalkshowVideo(c *gin.Context) {
	var inputs []model.TalkshowVideoInput

	if err := c.ShouldBindJSON(&inputs); err != nil {
		c.JSON(400, utils.ErrorMessages(utils.ErrorBadRequest, err))
		return
	}

	talkshows, err := h.talkshowService.BulkCreateTalkshow(inputs)

	if err != nil {
		c.JSON(500, utils.ErrorMessages(utils.ErrorInternalServer, err))
		return
	}

	c.JSON(201, talkshows)

}
