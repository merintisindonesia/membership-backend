package handler

import (
	"fmt"

	"github.com/gin-gonic/gin"
	"gitlab.com/merintisindonesia/membership-backend/model"
	"gitlab.com/merintisindonesia/membership-backend/service"
	"gitlab.com/merintisindonesia/membership-backend/utils"
)

type (
	examHandler struct {
		examService service.ExamService
	}
)

func NewExamHandler(examService service.ExamService) *examHandler {
	return &examHandler{examService: examService}
}

func (h *examHandler) GetAllExams(c *gin.Context) {
	exams, err := h.examService.ShowAll()

	if err != nil {
		c.JSON(500, utils.ErrorMessages(utils.ErrorInternalServer, err))
		return
	}

	c.JSON(200, exams)
}

func (h *examHandler) ShowShuffleExamByMemberType(c *gin.Context) {
	// memberType := c.Params.ByName("membership_type_id")

	memberType, _ := c.Get("membership_type")
	memberStatus, _ := c.Get("membership_status")

	fmt.Println(memberType.(string))
	fmt.Println(memberStatus.(string))

	shuffleExams, err := h.examService.ShowShuffleExam(memberType.(string), memberStatus.(string))

	if err != nil {
		c.JSON(500, utils.ErrorMessages(utils.ErrorInternalServer, err))
		return
	}

	c.JSON(200, shuffleExams)
}

func (h *examHandler) BulKCreateExams(c *gin.Context) {
	var inputs []model.ExamInput

	if err := c.ShouldBindJSON(&inputs); err != nil {
		c.JSON(400, utils.ErrorMessages(utils.ErrorBadRequest, err))
		return
	}

	// fmt.Println(inputs)

	exams, err := h.examService.BulkCreateNewExam(inputs)

	if err != nil {
		c.JSON(500, utils.ErrorMessages(utils.ErrorInternalServer, err))
		return
	}

	c.JSON(201, exams)
}

func (h *examHandler) CreateNewExam(c *gin.Context) {
	var input model.ExamInput

	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(400, utils.ErrorMessages(utils.ErrorBadRequest, err))
		return
	}

	exam, err := h.examService.CreateNewExam(input)

	if err != nil {
		c.JSON(500, utils.ErrorMessages(utils.ErrorInternalServer, err))
		return
	}

	c.JSON(201, exam)
}
