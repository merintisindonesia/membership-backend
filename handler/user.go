package handler

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/merintisindonesia/membership-backend/model"
	"gitlab.com/merintisindonesia/membership-backend/service"
	"gitlab.com/merintisindonesia/membership-backend/utils"
)

type (
	userHandler struct {
		userService service.UserService
	}
)

func NewUserHandler(userService service.UserService) *userHandler {
	return &userHandler{userService: userService}
}

// register user
func (h *userHandler) RegisterNewUser(c *gin.Context) {
	var input model.UserRegisterInput

	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(400, utils.ErrorMessages(utils.ErrorBadRequest, err))
		return
	}

	user, err := h.userService.CreateUser(input)

	if err != nil {
		c.JSON(500, utils.ErrorMessages(utils.ErrorInternalServer, err))
		return
	}

	c.JSON(201, user)
}

func (h *userHandler) LoginUser(c *gin.Context) {
	var input model.UserLoginInput

	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(400, utils.ErrorMessages(utils.ErrorBadRequest, err))
		return
	}

	user, err := h.userService.LoginUser(input)

	if err != nil {
		c.JSON(500, utils.ErrorMessages(utils.ErrorInternalServer, err))
		return
	}

	c.JSON(200, user)
}

func (h *userHandler) CheckDataMemebershipByUserLogin(c *gin.Context) {
	id := c.Params.ByName("user_id")

	dataMembership, err := h.userService.CheckMembershipByUserID(id)

	if err != nil {
		c.JSON(500, utils.ErrorMessages(utils.ErrorInternalServer, err))
		return
	}

	c.JSON(200, dataMembership)
}
