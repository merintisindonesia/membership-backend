package handler

import (
	"errors"

	"github.com/gin-gonic/gin"
	"gitlab.com/merintisindonesia/membership-backend/model"
	"gitlab.com/merintisindonesia/membership-backend/service"
	"gitlab.com/merintisindonesia/membership-backend/utils"
)

type (
	refferalHandler struct {
		refferalService service.RefferalService
	}
)

func NewRefferalhandler(refferalService service.RefferalService) *refferalHandler {
	return &refferalHandler{refferalService: refferalService}
}

func (h *refferalHandler) GetRefferalByCode(c *gin.Context) {
	var code = c.Params.ByName("code")

	if code == "" {
		c.JSON(400, utils.ErrorMessages(utils.ErrorParameterNotFound, errors.New("please insert code parameter")))
		return
	}

	refferal, err := h.refferalService.GetRefferalByCode(code)

	if err != nil {
		c.JSON(500, utils.ErrorMessages(utils.ErrorInternalServer, err))
	}

	c.JSON(200, refferal)
}

func (h *refferalHandler) UseRefferalCode(c *gin.Context) {
	var code = c.Params.ByName("code")

	if code == "" {
		c.JSON(400, utils.ErrorMessages(utils.ErrorParameterNotFound, errors.New("please insert code parameter")))
		return
	}

	refferal, err := h.refferalService.UseRefferalByCode(code)

	if err != nil {
		c.JSON(500, utils.ErrorMessages(utils.ErrorInternalServer, err))
	}

	c.JSON(200, refferal)
}

func (h *refferalHandler) GetAllRefferals(c *gin.Context) {
	refferals, err := h.refferalService.GetAllRefferal()

	if err != nil {
		c.JSON(500, utils.ErrorMessages(utils.ErrorInternalServer, err))
	}

	c.JSON(200, refferals)
}

func (h *refferalHandler) CreateNewRefferal(c *gin.Context) {
	var input model.RefferalInput

	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(400, utils.ErrorMessages(utils.ErrorBadRequest, err))
		return
	}

	refferal, err := h.refferalService.CreateNewRefferal(input)

	if err != nil {
		c.JSON(500, utils.ErrorMessages(utils.ErrorInternalServer, err))
		return
	}

	c.JSON(200, refferal)
}

func (h *refferalHandler) UpdateRefferalById(c *gin.Context) {
	var id = c.Params.ByName("refferal_id")

	var input model.RefferalUpdateInput

	if id == "" {
		c.JSON(400, utils.ErrorMessages(utils.ErrorParameterNotFound, errors.New("please insert id refferal parameter")))
		return
	}

	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(400, utils.ErrorMessages(utils.ErrorBadRequest, errors.New("please insert id refferal parameter")))
		return
	}

	refferal, err := h.refferalService.UpdateRefferalById(id, input)

	if err != nil {
		c.JSON(500, utils.ErrorMessages(utils.ErrorInternalServer, err))
		return
	}

	c.JSON(200, refferal)
}

func (h *refferalHandler) ChangeToInactiveRefferal(c *gin.Context) {
	var id = c.Params.ByName("refferal_id")

	if id == "" {
		c.JSON(400, utils.ErrorMessages(utils.ErrorParameterNotFound, errors.New("please insert id refferal parameter")))
		return
	}

	refferal, err := h.refferalService.InactiveRefferalById(id)

	if err != nil {
		c.JSON(500, utils.ErrorMessages(utils.ErrorInternalServer, err))
		return
	}

	c.JSON(200, refferal)
}
