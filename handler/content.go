package handler

import (
	"errors"

	"github.com/gin-gonic/gin"
	"gitlab.com/merintisindonesia/membership-backend/model"
	"gitlab.com/merintisindonesia/membership-backend/service"
	"gitlab.com/merintisindonesia/membership-backend/utils"
)

type (
	contentHandler struct {
		contentService service.ContentService
	}
)

func NewContentHandler(contentService service.ContentService) *contentHandler {
	return &contentHandler{contentService: contentService}
}

/*

get content video all
get content video id

create content video / modules (chechking membership type in db yes or not)
update modules
cannot delete modules
*/

func (h *contentHandler) GetAllContents(c *gin.Context) {
	contents, err := h.contentService.ShowAll()

	if err != nil {
		c.JSON(500, utils.ErrorMessages(utils.ErrorInternalServer, err))
		return
	}

	c.JSON(200, contents)
}

func (h *contentHandler) GetContentByID(c *gin.Context) {
	id := c.Params.ByName("content_id")

	content, err := h.contentService.ShowByID(id)

	if err != nil {
		c.JSON(500, utils.ErrorMessages(utils.ErrorInternalServer, err))
		return
	}

	c.JSON(200, content)
}

func (h *contentHandler) GetContentByMemberID(c *gin.Context) {
	id := c.Params.ByName("membership_type_id")

	contents, err := h.contentService.ShowByMemberTypeID(id)

	if err != nil {
		c.JSON(500, utils.ErrorMessages(utils.ErrorInternalServer, err))
		return
	}

	c.JSON(200, contents)
}

func (h *contentHandler) BulkCreateContents(c *gin.Context) {
	var inputs []model.ContentInput

	if err := c.ShouldBindJSON(&inputs); err != nil {
		c.JSON(400, utils.ErrorMessages(utils.ErrorBadRequest, err))
		return
	}

	contents, err := h.contentService.BulkCreateContent(inputs)

	if err != nil {
		c.JSON(500, utils.ErrorMessages(utils.ErrorInternalServer, err))
		return
	}

	c.JSON(201, contents)

}

func (h *contentHandler) CreateNewContent(c *gin.Context) {
	var input model.ContentInput

	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(400, utils.ErrorMessages(utils.ErrorBadRequest, err))
		return
	}

	content, err := h.contentService.CreateContent(input)

	if err != nil {
		c.JSON(500, utils.ErrorMessages(utils.ErrorInternalServer, err))
		return
	}

	c.JSON(201, content)
}

func (h *contentHandler) UpdateContentByID(c *gin.Context) {
	var updateInput model.ContentUpdateInput

	id := c.Params.ByName("content_id")

	if err := c.ShouldBindJSON(&updateInput); err != nil {
		c.JSON(400, utils.ErrorMessages(utils.ErrorBadRequest, err))
		return
	}

	if updateInput.MembershipTypeID > 4 {
		c.JSON(400, utils.ErrorMessages(utils.ErrorBadRequest, errors.New("please insert corrent membership type ID")))
		return
	}

	content, err := h.contentService.UpdateByID(id, updateInput)

	if err != nil {
		c.JSON(500, utils.ErrorMessages(utils.ErrorInternalServer, err))
		return
	}

	c.JSON(200, content)
}
