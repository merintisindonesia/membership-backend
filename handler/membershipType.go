package handler

import (
	"errors"

	"github.com/gin-gonic/gin"
	"gitlab.com/merintisindonesia/membership-backend/service"
	"gitlab.com/merintisindonesia/membership-backend/utils"
)

/*
phase 1 (hard code)
get all membership type

// phase 2
can insert / create if login is admin
can update / create if login is admin

no delete
*/

type (
	membershipTypeHandler struct {
		membersipTypeService service.MembershipTypeService
	}
)

func NewMembershipTypeHandler(membersipTypeService service.MembershipTypeService) *membershipTypeHandler {
	return &membershipTypeHandler{membersipTypeService: membersipTypeService}
}

func (h *membershipTypeHandler) GetAllMembershipType(c *gin.Context) {
	membershipTypes, err := h.membersipTypeService.ShowAll()

	if err != nil {
		c.JSON(500, utils.ErrorMessages(utils.ErrorInternalServer, err))
		return
	}

	c.JSON(200, membershipTypes)
}

func (h *membershipTypeHandler) GetAllMembershipTypeByID(c *gin.Context) {
	idMemberType := c.Params.ByName("membership_type_id")

	if idMemberType == "" {
		c.JSON(400, utils.ErrorMessages(utils.ErrorParameterNotFound, errors.New("error perameter not found, please insert parameter")))
		return
	}

	membeshipType, err := h.membersipTypeService.ShowAllByID(idMemberType)

	if err != nil {
		c.JSON(500, utils.ErrorMessages(utils.ErrorInternalServer, err))
		return
	}

	c.JSON(200, membeshipType)

}
