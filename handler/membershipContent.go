package handler

import (
	"errors"

	"github.com/gin-gonic/gin"
	"gitlab.com/merintisindonesia/membership-backend/model"
	"gitlab.com/merintisindonesia/membership-backend/service"
	"gitlab.com/merintisindonesia/membership-backend/utils"
)

type (
	membershipContenthandler struct {
		membershipContentService service.MembershipContentService
	}
)

func NewMembershipContentHandler(membershipContentService service.MembershipContentService) *membershipContenthandler {
	return &membershipContenthandler{membershipContentService: membershipContentService}
}

func (h *membershipContenthandler) ShowAllContentMembershipByID(c *gin.Context) {
	membershipID, ok := c.Get("membership_id")

	if !ok {
		c.JSON(401, utils.ErrorMessages(utils.ErrorUnauthorizeUser, errors.New(("error user unauthorize"))))
		return
	}

	membershipContents, err := h.membershipContentService.ShowAllByMembershipID(membershipID.(string))

	if err != nil {
		c.JSON(500, utils.ErrorMessages(utils.ErrorInternalServer, err))
		return
	}

	c.JSON(200, membershipContents)
}

func (h *membershipContenthandler) UpdateMembershipContentNotesByID(c *gin.Context) {
	id := c.Params.ByName("membership_content_id")

	if id == "" || len(id) <= 0 {
		c.JSON(500, utils.ErrorMessages(utils.ErrorParameterNotFound, errors.New("error perameter not found, please insert parameter")))
		return
	}

	var notes model.UpdateNotesMembershipContent

	if err := c.ShouldBindJSON(&notes); err != nil {
		c.JSON(400, utils.ErrorMessages(utils.ErrorBadRequest, err))
		return
	}

	membershipContent, err := h.membershipContentService.UpdateNotesByContentID(id, notes.Notes)

	if err != nil {
		c.JSON(500, utils.ErrorMessages(utils.ErrorInternalServer, err))
		return
	}

	c.JSON(200, membershipContent)
}

func (h *membershipContenthandler) UpdateMembershipContentDoneByID(c *gin.Context) {
	id := c.Params.ByName("membership_content_id")

	if id == "" || len(id) <= 0 {
		c.JSON(500, utils.ErrorMessages(utils.ErrorParameterNotFound, errors.New("error perameter not found, please insert parameter")))
		return
	}

	membershipContent, err := h.membershipContentService.UpdateDoneByContentID(id)

	if err != nil {
		c.JSON(500, utils.ErrorMessages(utils.ErrorInternalServer, err))
		return
	}

	c.JSON(200, membershipContent)
}

func (h *membershipContenthandler) GetMembershipContentByID(c *gin.Context) {
	id := c.Params.ByName("membership_content_id")

	if id == "" || len(id) <= 0 {
		c.JSON(500, utils.ErrorMessages(utils.ErrorParameterNotFound, errors.New("error perameter not found, please insert parameter")))
		return
	}

	membershipContent, err := h.membershipContentService.GetMembershipContentByID(id)

	if err != nil {
		c.JSON(500, utils.ErrorMessages(utils.ErrorInternalServer, err))
		return
	}

	c.JSON(200, membershipContent)
}
