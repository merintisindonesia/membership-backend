package handler

import (
	"errors"

	"github.com/gin-gonic/gin"
	"gitlab.com/merintisindonesia/membership-backend/model"
	"gitlab.com/merintisindonesia/membership-backend/service"
	"gitlab.com/merintisindonesia/membership-backend/utils"
)

type (
	membershipExamHandler struct {
		membershipExamService service.MembershipExamService
	}
)

func NewMembershipExamHandler(membershipExamService service.MembershipExamService) *membershipExamHandler {
	return &membershipExamHandler{membershipExamService: membershipExamService}
}

func (h *membershipExamHandler) GetAllByMemberLogin(c *gin.Context) {
	memberID, _ := c.Get("membership_id")

	memberExams, err := h.membershipExamService.GetAllByMemberID(memberID.(string))

	if err != nil {
		c.JSON(500, utils.ErrorMessages(utils.ErrorInternalServer, err))
		return
	}

	c.JSON(200, memberExams)
}

func (h *membershipExamHandler) CreateNewMemberExam(c *gin.Context) {
	memberID, _ := c.Get("membership_id")

	memberExam, err := h.membershipExamService.CreateNewMemberExam(memberID.(string))

	if err != nil {
		c.JSON(500, utils.ErrorMessages(utils.ErrorInternalServer, err))
		return
	}

	c.JSON(201, memberExam)
}

func (h *membershipExamHandler) UpdateDoneExamMember(c *gin.Context) {
	memberExamID := c.Params.ByName("membership_exam_id")

	var input model.MembershipExamInput

	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(400, utils.ErrorMessages(utils.ErrorBadRequest, err))
		return
	}

	if memberExamID == "" {
		c.JSON(400, utils.ErrorMessages(utils.ErrorParameterNotFound, errors.New("please insert membership exam id parameter")))
		return
	}

	memberExam, err := h.membershipExamService.UpdateForDone(input, memberExamID)

	if err != nil {
		c.JSON(500, utils.ErrorMessages(utils.ErrorInternalServer, err))
		return
	}

	c.JSON(200, memberExam)

}

func (h *membershipExamHandler) DeleteMemberExamById(c *gin.Context) {
	memberExamID := c.Params.ByName("membership_exam_id")

	if memberExamID == "" {
		c.JSON(400, utils.ErrorMessages(utils.ErrorParameterNotFound, errors.New("please insert membership exam id parameter")))
		return
	}

	deleted, err := h.membershipExamService.DeleteMembershipExamById(memberExamID)

	if err != nil {
		c.JSON(500, utils.ErrorMessages(utils.ErrorInternalServer, err))
		return
	}

	c.JSON(200, deleted)
}

func (h *membershipExamHandler) BulkDeleteMemberExam(c *gin.Context) {
	var input model.BulkDeleteInput

	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(400, utils.ErrorMessages(utils.ErrorBadRequest, err))
		return
	}

	deletes, err := h.membershipExamService.BulkDeleteMembershipExam(input)
	if err != nil {
		c.JSON(500, utils.ErrorMessages(utils.ErrorInternalServer, err))
		return
	}

	c.JSON(200, deletes)
}

func (h *membershipExamHandler) GetMemberExamId(c *gin.Context) {
	id := c.Params.ByName("membership_exam_id")

	if id == "" {
		c.JSON(400, utils.ErrorMessages(utils.ErrorParameterNotFound, errors.New("please insert membership exam id parameter")))
		return
	}

	memberExam, err := h.membershipExamService.GetMemberExamById(id)
	if err != nil {
		c.JSON(500, utils.ErrorMessages(utils.ErrorInternalServer, err))
		return
	}

	c.JSON(200, memberExam)
}
