package migration

import "time"

// masih belum fix, on progress

// 12 table

type (
	// =========================
	// user access
	// =========================
	User struct {
		Id              int              `gorm:"primaryKey" json:"id"`
		NmLengkap       string           `json:"nm_lengkap"`
		Email           string           `json:"email"`
		NoHP            string           `json:"no_hp"`
		Password        string           `json:"password"`
		RememberToken   string           `json:"remember_token"`
		DataMemberships []DataMembership `json:"data_membership"`
		UserDetail      UserDetail       `json:"user_detail"`
	}

	UserDetail struct {
		Id          string    `gorm:"primaryKey" json:"id"`
		UserID      int       `gorm:"index" json:"user_id"`
		PlaceBirth  string    `json:"place_birth"`
		DateBirth   time.Time `json:"date_birth"`
		Gender      string    `json:"gender"`
		FullAddress string    `json:"full_address"`
	}

	DataMembership struct {
		Id                 string              `gorm:"primaryKey" json:"id"`
		UserID             int                 `json:"user_id" gorm:"index"`
		Status             string              `json:"status"`
		Type               string              `json:"type"`
		ReasonJoinMember   string              `json:"reason_join_member"`
		IsPaid             bool                `json:"is_paid"`
		CreatedAt          time.Time           `json:"created_at"`
		ExpiredDate        time.Time           `json:"expired_date"`
		Deleted            bool                `json:"deleted"`
		CertificateName    string              `json:"certificate_name"`
		MembershipContents []MembershipContent `json:"membership_contents" gorm:"foreignKey:MembershipID"`
		MembershipExams    []MembershipExam    `json:"membership_exams" gorm:"foreignKey:MembershipID"`
		Certificate        Certificate         `gorm:"foreignKey:MembershipID" json:"certificate"`
		Payment            Payment             `gorm:"foreignKey:MembershipID" json:"payment"`
	}

	Payment struct {
		Id              string `gorm:"primaryKey"`
		MembershipID    string `json:"membership_id" gorm:"index"`
		OrderID         string `json:"order_id"`
		PaymentMethod   string
		PaymentProofURL string
		Amount          uint
		UserSender      string
		BankSender      string
		Status          string    `json:"status"` // accept, pending, reject
		CreatedAt       time.Time `json:"created_at"`
	}

	MembershipContent struct {
		Id           string    `gorm:"primaryKey" json:"id"`
		MembershipID string    `gorm:"index" json:"membership_id"`
		ContentID    string    `gorm:"index" json:"content_id"`
		Notes        string    `json:"notes"`
		Done         bool      `json:"done"`
		CreatedAt    time.Time `json:"created_at"`
		UpdatedAt    time.Time `json:"updated_at"`
	}

	MembershipExam struct {
		Id           string `json:"id" gorm:"primaryKey"`
		MembershipID string `gorm:"index" json:"membership_id"`
		Score        int
		CountGetExam int
		CreatedAt    time.Time
		TimeLength   time.Time
	}

	Certificate struct {
		Id             string    `json:"id" gorm:"primaryKey"`
		MembershipID   string    `gorm:"index" json:"membership_id"`
		CertificateURL string    `json:"certoficate_url"`
		CreatedAt      time.Time `json:"created_at"`
	}

	// =========================
	// another access
	// =========================
	MembershipType struct {
		Id        int       `gorm:"primaryKey" json:"id"`
		Type      string    `json:"type"`
		Status    string    `json:"status"`
		CreatedAt time.Time `json:"created_at"`
		UpdatedAt time.Time `json:"updated_at"`
		Contents  []Content `json:"contents" gorm:"foreignKey:MembershipTypeID"`
		Exams     []Exam    `json:"exams" gorm:"foreignKey:MembershipTypeID"`
	}

	Content struct {
		Id               string `json:"id" gorm:"primaryKey"`
		MembershipTypeID int    `gorm:"index" json:"membership_type_id"`
		Title            string
		Description      string
		Position         int
		VIdeoURL         string
		CreatedAt        time.Time
		UpdatedAt        time.Time
		Deleted          bool
	}

	Exam struct {
		Id               string `gorm:"primaryKey" json:"id"`
		MembershipTypeID int    `gorm:"index" json:"membership_type_id"`
		Question         string
		Choice           string // dipecah dengan ","
		CorrectAnswer    string
		CreatedAt        time.Time
	}

	TalkshowVideo struct {
		Id          string `gorm:"primaryKey" json:"id"`
		Title       string `json:"title"`
		Description string `json:"description"`
		VideoURL    string `json:"video_url"`
	}

	Refferal struct {
		Id         string `json:"id" gorm:"primaryKey"`
		Code       string `json:"code"`
		Discount   int    `json:"discount"` // convert to % ex: 10 = 10%
		Limit      int    `json:"limit"`
		UsedCode   int    `json:"used_code"`
		ActiveCode bool   `json:"active_code"` // if inactive change false
	}
)
