package utils

import "fmt"

var (
	SoftDelete = "data still in database (soft deleted)"
	Delete     = "data success remove from database"
)

type DeleteMessageModel struct {
	Status  string `json:"status"`
	Message string `json:"message"`
	Deleted string `json:"deleted"`
}

type SuccessPaidModel struct {
	Status  string `json:"status"`
	Message string `json:"message"`
}

func DeleteMessage(deleteMessage string, typeDelete string) *DeleteMessageModel {
	return &DeleteMessageModel{
		Status:  "success",
		Message: deleteMessage,
		Deleted: typeDelete,
	}
}

func SuccessPaidMembership(id string) *SuccessPaidModel {
	return &SuccessPaidModel{
		Status:  "success paid confirmation verification",
		Message: fmt.Sprintf("membership id %s confirmation payment accept, please login to membership.merintisindonesia.com", id),
	}
}
