package utils

import "strings"

func SplitStrToArrayStr(value string) []string {
	result := strings.Split(value, ";")

	return result
}

func JoinArrToString(vals []string) string {
	result := strings.Join(vals, ";")

	return result
}
