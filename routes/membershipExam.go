package routes

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/merintisindonesia/membership-backend/handler"
	"gitlab.com/merintisindonesia/membership-backend/repository"
	"gitlab.com/merintisindonesia/membership-backend/service"
)

var (
	membershipExamRepo    = repository.NewMembershipExamRepository(db)
	membershipExamService = service.NewMembershipExamService(membershipExamRepo, membershipQuestExamRepo, examService, dataMembershipRepo)
	membershipExamHandler = handler.NewMembershipExamHandler(membershipExamService)
)

func MembershipExamRoute(r *gin.Engine) {
	v1 := r.Group("/v1")
	{
		// get all data exam by user login data_member_id
		v1.GET("/membership_exams", mainMiddleware, membershipExamHandler.GetAllByMemberLogin)
		v1.GET("/membership_exams/:membership_exam_id", mainMiddleware, membershipExamHandler.GetMemberExamId)

		// post if user take exam new
		v1.POST("/membership_exams", mainMiddleware, membershipExamHandler.CreateNewMemberExam)

		// upload score if exam get done
		v1.POST("/membership_exams/:membership_exam_id", mainMiddleware, membershipExamHandler.UpdateDoneExamMember)

		//login for checking user still exam or finish

		// if user score > 0 , then user finish exam
		v1.DELETE("/membership_exams/:membership_exam_id", mainMiddleware, adminDevMiddleware, membershipExamHandler.DeleteMemberExamById)
		v1.DELETE("/membership_exams/bulk", mainMiddleware, adminDevMiddleware, membershipExamHandler.BulkDeleteMemberExam)
	}
}
