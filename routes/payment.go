package routes

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/merintisindonesia/membership-backend/repository"
)

var (
	paymentRepo = repository.NewPaymentRepository(db)
)

func PaymentRoute(c *gin.Engine) {

	v1 := c.Group("/v1")
	{
		v1.GET("/payments")
		v1.POST("/payments/upload_proof")
	}
}
