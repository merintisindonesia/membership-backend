package routes

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/merintisindonesia/membership-backend/handler"
	"gitlab.com/merintisindonesia/membership-backend/repository"
	"gitlab.com/merintisindonesia/membership-backend/service"
)

var (
	membershipTypeRepo    = repository.NewMembershipTypeRepository(db)
	membershipTypeService = service.NewMembershipTypeService(membershipTypeRepo)
	membershipTypeHandler = handler.NewMembershipTypeHandler(membershipTypeService)
)

func MembershipTypeRoute(r *gin.Engine) {
	// db.AutoMigrate(&migration.User{}, &migration.UserDetail{}, &migration.DataMembership{}, &migration.Payment{}, &migration.MembershipContent{}, &migration.MembershipExam{}, &migration.Certificate{}, &migration.MembershipType{}, &migration.Content{}, &migration.Exam{}, &migration.TalkshowVideo{}, &migration.Refferal{})

	v1 := r.Group("/v1")
	{
		v1.GET("/membership_type", membershipTypeHandler.GetAllMembershipType)
		v1.GET("/membership_type/:membership_type_id", membershipTypeHandler.GetAllMembershipTypeByID)
	}
}
