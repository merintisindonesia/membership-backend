package routes

import "github.com/gin-gonic/gin"

var ()

func CertificateRoute(r *gin.Engine) {
	v1 := r.Group("/v1")
	{
		// get by one certificate by user login membership ID
		v1.GET("/certificates", mainMiddleware)

		// post if user pass exam with score 75
		v1.POST("/certificates", mainMiddleware)
	}
}
