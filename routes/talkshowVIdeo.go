package routes

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/merintisindonesia/membership-backend/handler"
	"gitlab.com/merintisindonesia/membership-backend/repository"
	"gitlab.com/merintisindonesia/membership-backend/service"
)

var (
	talkshowRepo    = repository.NewTalkshowVideoRepository(db)
	talkshowService = service.NewTalkshowVideoService(talkshowRepo)
	talkshowHandler = handler.NewTalkshowVideoHandler(talkshowService)
)

func TalkshowVideoRoute(r *gin.Engine) {
	v1 := r.Group("/v1")
	{
		v1.GET("/talkshow_videos", mainMiddleware, talkshowHandler.GetAllTalkshow)

		// FOR DEVELOPER / ADMIN ONLY

		v1.POST("/talkshow_videos", mainMiddleware, adminDevMiddleware, talkshowHandler.CreateNewTalkshow)

		// v1.PUT("/talkshow_videos/:talkshow_video_id")

		v1.POST("/talkshow_videos/bulk", mainMiddleware, adminDevMiddleware, talkshowHandler.BulkCreateTalkshowVideo)
	}
}
