package routes

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/merintisindonesia/membership-backend/handler"
	"gitlab.com/merintisindonesia/membership-backend/repository"
	"gitlab.com/merintisindonesia/membership-backend/service"
)

var (
	examRepo    = repository.NewExamRepository(db)
	examService = service.NewExamService(examRepo)
	examHandler = handler.NewExamHandler(examService)
)

func ExamRoute(r *gin.Engine) {
	v1 := r.Group("/v1")
	{
		// for admin and dev
		v1.GET("/exams", mainMiddleware, adminDevMiddleware, examHandler.GetAllExams)

		// for user start get exam with shuffle question
		v1.GET("/exams/start", mainMiddleware, examHandler.ShowShuffleExamByMemberType)

		// FOR DEVELOPER / ADMIN

		// post create exam by admin / dev
		v1.POST("/exams/bulk", mainMiddleware, adminDevMiddleware, examHandler.BulKCreateExams)

		v1.POST("/exams", mainMiddleware, adminDevMiddleware, examHandler.CreateNewExam)

		// update for exam
		// susah update choice
		// v1.PUT("/exams/:exam_id")

		// delete exam
		v1.DELETE("/exams/:exam_id", mainMiddleware, adminDevMiddleware)
	}
}
