package routes

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/merintisindonesia/membership-backend/handler"
	"gitlab.com/merintisindonesia/membership-backend/repository"
	"gitlab.com/merintisindonesia/membership-backend/service"
)

var (
	userRepo           = repository.NewUserRepository(db)
	dataMembershipRepo = repository.NewDataMembersipRepository(db)

	dataMembershipService = service.NewDataMembershipService(authService, userRepo, dataMembershipRepo, userDetailRepo, membershipContentRepo, contentRepo, paymentRepo, midtransService, emailService)

	dataMembershipHandler = handler.MewDataMembershipHandler(dataMembershipService)
)

func DataMembershipRoute(r *gin.Engine) {
	v1 := r.Group("/v1")
	{
		// ALL ACCESS TOKEN / AUTH TOKEN IN HERE ROUTE
		v1.GET("/memberships/profile", mainMiddleware, dataMembershipHandler.GetProfileMembership)

		v1.POST("/memberships/login", dataMembershipHandler.LoginMembership)

		v1.POST("/memberships/create", dataMembershipHandler.CreateMembershipUser)

		// perlu diubah apinya tanpa parameter
		v1.PUT("/memberships/:membership_id/paid", mainMiddleware, adminDevMiddleware, dataMembershipHandler.UpdatePaidAndBulkContentMember)

		// post untuk menerima pembayaran notifikasi midtrans
		v1.POST("/memberships/account/paid", dataMembershipHandler.GetNotificationPaymentMembership)

		v1.GET("/memberships/content", mainMiddleware)

		// dipecah ke route membership certificate
		v1.POST("/memberships/certificate", mainMiddleware)
	}
}
