package routes

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/merintisindonesia/membership-backend/handler"
	"gitlab.com/merintisindonesia/membership-backend/repository"
	"gitlab.com/merintisindonesia/membership-backend/service"
)

var (
	contentRepo    = repository.NewContentRepository(db)
	contentService = service.NewContentService(contentRepo)
	contentHandler = handler.NewContentHandler(contentService)
)

func ContentRoute(r *gin.Engine) {
	v1 := r.Group("/v1")
	{
		v1.GET("/contents", mainMiddleware, contentHandler.GetAllContents)

		v1.GET("/contents/:content_id", mainMiddleware, contentHandler.GetContentByID)

		// get content by membership type table
		v1.GET("/contents/membership_type/:membership_type_id", contentHandler.GetContentByMemberID)

		// FOR DEVELOPER / ADMIN

		// create content data
		v1.POST("/contents/bulk", mainMiddleware, adminDevMiddleware, contentHandler.BulkCreateContents)

		v1.POST("/contents", mainMiddleware, adminDevMiddleware, contentHandler.CreateNewContent)

		// update name, description or video link
		v1.PUT("/contents/:content_id", mainMiddleware, adminDevMiddleware, contentHandler.UpdateContentByID)

		// deleted soft, because integrate with table membership_content
		v1.DELETE("/contents/:content_id") // in progress
	}
}
