package routes

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/merintisindonesia/membership-backend/handler"
	"gitlab.com/merintisindonesia/membership-backend/service"
)

var (
	userService = service.NewUserService(userRepo, dataMembershipRepo, userDetailRepo, paymentRepo, *midtransService)
	userHandler = handler.NewUserHandler(userService)
)

func UserRoute(r *gin.Engine) {
	v1 := r.Group("/v1")
	{
		v1.GET("/users/:user_id/membership", userHandler.CheckDataMemebershipByUserLogin)

		v1.POST("/users/register", userHandler.RegisterNewUser)
		v1.POST("/users/login", userHandler.LoginUser)
	}
}
