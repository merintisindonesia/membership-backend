package routes

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/merintisindonesia/membership-backend/handler"
	"gitlab.com/merintisindonesia/membership-backend/repository"
	"gitlab.com/merintisindonesia/membership-backend/service"
)

var (
	membershipQuestExamRepo    = repository.NewMembershipQuestExamRepository(db)
	membershipQuestExamService = service.NewMembershipQuestExamService(membershipQuestExamRepo)
	membershipQuestExamHandler = handler.NewMemberQuestExamHandler(membershipQuestExamService)
)

func MembershipQuestExamRoute(c *gin.Engine) {

	v1 := c.Group("/v1")
	{
		v1.GET("/membership_question_exams/membership_exam/:membership_exam_id", mainMiddleware, membershipQuestExamHandler.GetAllExamByMemberExamId)

		v1.PATCH("/membership_question_exams/:membership_question_exam_id/answer", mainMiddleware, membershipQuestExamHandler.UpdateAnswerQuestionExam)
	}
}
