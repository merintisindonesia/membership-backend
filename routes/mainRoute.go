package routes

import (
	"gitlab.com/merintisindonesia/membership-backend/auth"
	"gitlab.com/merintisindonesia/membership-backend/config"
	"gitlab.com/merintisindonesia/membership-backend/middleware"
	"gitlab.com/merintisindonesia/membership-backend/service"
)

var (
	db                 = config.ConnectionDB()
	authService        = auth.NewAuthService()
	mainMiddleware     = middleware.Middleware(authService)
	adminDevMiddleware = middleware.AdminDevMiddleware()
	midtransService    = service.NewMidtransService()
	emailService       = service.NewEmailService()
)
