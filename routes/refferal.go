package routes

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/merintisindonesia/membership-backend/handler"
	"gitlab.com/merintisindonesia/membership-backend/repository"
	"gitlab.com/merintisindonesia/membership-backend/service"
)

var (
	refferalRepo    = repository.NewRefferalRepository(db)
	refferalService = service.NewRefferalService(refferalRepo)
	refferalHandler = handler.NewRefferalhandler(refferalService)
)

func RefferalRoute(r *gin.Engine) {
	v1 := r.Group("/v1")
	{
		v1.GET("/refferals/:code", refferalHandler.GetRefferalByCode)

		v1.POST("/refferals/:code/use", refferalHandler.UseRefferalCode)

		// for admin / developer only
		v1.GET("/refferals", mainMiddleware, adminDevMiddleware, refferalHandler.GetAllRefferals)

		v1.POST("/refferals", mainMiddleware, adminDevMiddleware, refferalHandler.CreateNewRefferal)

		v1.PUT("/refferals/:refferal_id", mainMiddleware, adminDevMiddleware, refferalHandler.UpdateRefferalById)

		v1.PATCH("/refferals/:refferal_id/inactive", mainMiddleware, adminDevMiddleware, refferalHandler.ChangeToInactiveRefferal)

	}
}
