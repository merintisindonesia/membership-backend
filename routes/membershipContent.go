package routes

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/merintisindonesia/membership-backend/handler"
	"gitlab.com/merintisindonesia/membership-backend/repository"
	"gitlab.com/merintisindonesia/membership-backend/service"
)

var (
	membershipContentRepo    = repository.NewMembershipContentRepository(db)
	membershipContentService = service.NewMembershipContentService(membershipContentRepo, contentRepo)
	membershipContenrHandler = handler.NewMembershipContentHandler(membershipContentService)
)

func MembershipContentRoute(r *gin.Engine) {
	v1 := r.Group("/v1")
	{
		// get all content by user login data_membership_id
		v1.GET("/membership_contents", mainMiddleware, membershipContenrHandler.ShowAllContentMembershipByID)
		v1.GET("/membership_contents/:membership_content_id", mainMiddleware, membershipContenrHandler.GetMembershipContentByID)

		// put for updates jika updates notes dan done video
		v1.PUT("/membership_contents/:membership_content_id/notes", mainMiddleware, membershipContenrHandler.UpdateMembershipContentNotesByID)
		v1.PUT("/membership_contents/:membership_content_id/done", mainMiddleware, membershipContenrHandler.UpdateMembershipContentDoneByID)

		// auto migrate if user finish payment (phase 2)
		v1.POST("/membership_content/insert_success_membership")

	}
}
