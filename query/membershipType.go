package query

/*
business flow:
- create table
- insert fixed membershipType (4 type)
- get All membershipType

*/

const (
	createTable = `CREATE TABLE IF NOT EXISTS membership_types (
		id serial PRIMARY KEY,
		type VARCHAR ( 50 ) NOT NULL,
		status VARCHAR ( 50 ) NOT NULL,
		created_at TIMESTAMP NOT NULL,
		updated_at TIMESTAMP NOT NULL,
	)`

	insertData = `INSERT INTO membership_types (type, status, created_at, updated_at)`
)
