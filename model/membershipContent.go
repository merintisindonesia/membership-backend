package model

import "time"

type (
	MembershipContent struct {
		Id           string    `json:"id"`
		MembershipID string    `json:"membership_id"`
		ContentID    string    `json:"content_id"`
		Notes        string    `json:"notes"`
		Done         bool      `json:"done"`
		CreatedAt    time.Time `json:"created_at"`
		UpdatedAt    time.Time `json:"updated_at"`
	}

	MembershipContentWithContent struct {
		Id           string    `json:"id"`
		MembershipID string    `json:"membership_id"`
		ContentID    string    `json:"content_id"`
		Notes        string    `json:"notes"`
		Done         bool      `json:"done"`
		Title        string    `json:"title"`       // module 1, mudul 2 , bla
		Description  string    `json:"description"` // judul module "growth midset, dll"
		Position     int       `json:"position"`
		VideoURL     string    `json:"video_url"`
		CreatedAt    time.Time `json:"created_at"`
		UpdatedAt    time.Time `json:"updated_at"`
	}

	UpdateNotesMembershipContent struct {
		Notes string `json:"notes"`
	}
)
