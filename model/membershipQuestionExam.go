package model

import "gitlab.com/merintisindonesia/membership-backend/utils"

type (
	MembershipQuestionExam struct {
		Id               string `json:"id" gorm:"primaryKey"`
		MembershipExamId string `json:"membership_exam_id"`
		Question         string `json:"question"`
		Choice           string `json:"choice"`
		CorrectAnswer    string `json:"correct_answer"`
		Answer           string `json:"answer"`
	}

	MembershipQuestionExamOutput struct {
		Id               string   `json:"id"`
		MembershipExamId string   `json:"membership_exam_id"`
		Question         string   `json:"question"`
		Choice           []string `json:"choice"`
		CorrectAnswer    string   `json:"correct_answer"`
		Answer           string   `json:"answer"`
	}

	MembershipQuestExamInputAnswer struct {
		Answer string `json:"answer" binding:"required"`
	}
)

func (mqe *MembershipQuestionExam) ToOutput() *MembershipQuestionExamOutput {
	return &MembershipQuestionExamOutput{
		Id:               mqe.Id,
		MembershipExamId: mqe.MembershipExamId,
		Question:         mqe.Question,
		Choice:           utils.SplitStrToArrayStr(mqe.Choice),
		CorrectAnswer:    mqe.CorrectAnswer,
		Answer:           mqe.Answer,
	}
}
