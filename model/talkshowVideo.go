package model

type (
	TalkshowVideo struct {
		Id          string `gorm:"primaryKey" json:"id"`
		Title       string `json:"title"`
		Description string `json:"description"`
		VideoURL    string `json:"video_url"`
		ImageURL    string `json:"image_url"`
	}

	TalkshowVideoInput struct {
		Title       string `json:"title" binding:"required"`
		Description string `json:"description" binding:"required"`
		VideoURL    string `json:"video_url" binding:"required"`
		ImageURL    string `json:"image_url" binding:"required"`
	}
)
