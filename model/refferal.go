package model

type (
	Refferal struct {
		Id         string `json:"id" gorm:"primaryKey"`
		Code       string `json:"code"`
		Discount   int    `json:"discount"` // convert to % ex: 10 = 10%
		Limit      int    `json:"limit"`
		UsedCode   int    `json:"used_code"`
		ActiveCode bool   `json:"active_code"` // if inactive change false
	}

	RefferalResponse struct {
		Status  string                 `json:"status"`
		Details RefferalDetailResponse `json:"details"`
	}

	RefferalDetailResponse struct {
		Code     string `json:"code"`
		Discount int    `json:"discount"`
		Message  string `json:"message"`
	}

	// Reffreal Input
	RefferalInput struct {
		Code     string `json:"code" binding:"required"`
		Discount int    `json:"discount" bidning:"required"`
		Limit    int    `json:"limit" binding:"required"`
	}

	RefferalUpdateInput struct {
		Code     string `json:"code"`
		Discount int    `json:"discount"`
		Limit    int    `json:"limit"`
	}

	RefferalSuccessUsedResponse struct {
		Status  string `json:"status"` // success / error
		Code    string `json:"code"`
		Message string `json:"message"`
	}
)
