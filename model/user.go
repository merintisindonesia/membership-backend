package model

import "time"

type (
	User struct {
		Id            int    `json:"id"`
		NmLengkap     string `json:"nm_lengkap"`
		Email         string `json:"email"`
		NoHP          string `json:"no_hp"`
		Password      string `json:"password"`
		RememberToken string `json:"remember_token"`
	}

	UserMembershipResponse struct {
		Id             int            `json:"id"`
		NmLengkap      string         `json:"nm_lengkap"`
		Email          string         `json:"email"`
		NoHP           string         `json:"no_hp"`
		UserDetail     UserDetail     `json:"user_detail"`
		DataMembership DataMembership `json:"data_membership"`
	}

	// user get data membership profile
	UserMembershipProfile struct {
		Id               int       `json:"id"`
		NmLengkap        string    `json:"nm_lengkap"`
		Email            string    `json:"email"`
		NoHP             string    `json:"no_hp"`
		Type             string    `json:"type"`
		Status           string    `json:"status"`
		ReasonJoinMember string    `json:"reason_join_member"`
		IsPaid           bool      `json:"is_paid"`
		CreatedAt        time.Time `json:"created_at"`
		ExpiredDate      time.Time `json:"expired_date"`
		CertificateName  string    `json:"certificate_name"`
		PlaceBirth       string    `json:"place_birth"`
		DateBirth        time.Time `json:"date_birth"`
		Gender           string    `json:"gender"`
		FullAddress      string    `json:"full_address"`
		Agency           string    `json:"agency"`
		PaymentURL       string    `json:"payment_url"`
	}

	UserRegisterInput struct {
		Id        int    `json:"id" binding:"required"`
		NmLengkap string `json:"nm_lengkap" binding:"required"`
		Email     string `json:"email" binding:"required"`
		NoHP      string `json:"no_hp" binding:"required"`
		Password  string `json:"password" binding:"required"`
	}

	UserRegisterResponse struct {
		Id        int    `json:"id"`
		NmLengkap string `json:"nm_lengkap"`
		Email     string `json:"email"`
		NoHP      string `json:"no_hp"`
	}

	UserCheckLoginResponse struct {
		Id        int    `json:"id"`
		NmLengkap string `json:"nm_lengkap"`
		Email     string `json:"email"`
		NoHP      string `json:"no_hp"`
	}

	UserLoginInput struct {
		Email    string `json:"email" binding:"required"`
		Password string `json:"password" binding:"required"`
	}
	UserSuccessLoginResponse struct {
		SetAuth string `json:"set_auth"` // "Authorization: <token>"
		Token   string `json:"token"`
		Role    string `json:"role"`
	}
)
