package model

import "time"

type (
	Payment struct {
		Id              string    `gorm:"primaryKey"`
		MembershipID    string    `json:"membership_id" gorm:"index"`
		OrderID         string    `json:"order_id"`
		PaymentURL      string    `json:"payment_url"`
		PaymentMethod   string    `json:"payment_method"`
		PaymentProofURL string    `json:"payment_proof_url"`
		Amount          uint      `json:"amount"`
		UserSender      string    `json:"user_sender"`
		BankSender      string    `json:"bank_sender"`
		Status          string    `json:"status"` // accept, pending, reject
		CreatedAt       time.Time `json:"created_at"`
	}

	SetInputPaymentUser struct {
		PaymentProofURL string `json:"payment_proof_url" binding:"required"`
		UserSender      string `json:"user_sender" binding:"required"`
		BankSender      string `json:"bank_sender" binding:"required"`
	}

	PaymentCount struct {
		Counting int `json:"counting"`
	}
)

//
