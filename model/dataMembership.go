package model

import "time"

type (
	DataMembership struct {
		Id               string    `gorm:"primaryKey" json:"id"`
		UserID           int       `json:"user_id" gorm:"index"`
		Status           string    `json:"status"`
		Type             string    `json:"type"`
		ReasonJoinMember string    `json:"reason_join_member"`
		IsPaid           bool      `json:"is_paid"`
		CreatedAt        time.Time `json:"created_at"`
		ExpiredDate      time.Time `json:"expired_date"`
		Deleted          bool      `json:"deleted"`
		CertificateName  string    `json:"certificate_name"`
	}

	DataMembershipWithpayment struct {
		Id               string    `gorm:"primaryKey" json:"id"`
		UserID           int       `json:"user_id" gorm:"index"`
		Status           string    `json:"status"`
		Type             string    `json:"type"`
		ReasonJoinMember string    `json:"reason_join_member"`
		IsPaid           bool      `json:"is_paid"`
		CreatedAt        time.Time `json:"created_at"`
		ExpiredDate      time.Time `json:"expired_date"`
		Deleted          bool      `json:"deleted"`
		CertificateName  string    `json:"certificate_name"`
		// payment part
		OrderID    string `json:"order_id"`
		Amount     uint   `json:"amount"`
		PaymentURL string `json:"payment_url"`
	}

	// with insert user details
	DataMembershipRegisterInput struct {
		UserID           int       `json:"user_id" binding:"required"`
		PlaceBirth       string    `json:"place_birth" binding:"required"`
		DateBirth        time.Time `json:"date_birth" binding:"required"`
		Gender           string    `json:"gender" binding:"required"`
		FullAddress      string    `json:"full_address" binding:"required"`
		Agency           string    `json:"agency"`
		ReasonJoinMember string    `json:"reason_join_member" binding:"required"`
		Status           string    `json:"status" binding:"required"`
		Type             string    `json:"type" binding:"required"`
		PaymentMethod    string    `json:"payment_method" binding:"required"`
		Amount           uint      `json:"amount" binding:"required"`
	}
)
