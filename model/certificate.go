package model

import "time"

type (
	Certificate struct {
		Id             string    `json:"id" gorm:"primaryKey"`
		MembershipID   string    `gorm:"index" json:"membership_id"`
		CertificateURL string    `json:"certoficate_url"`
		CreatedAt      time.Time `json:"created_at"`
	}
)
