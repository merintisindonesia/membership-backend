package model

import "time"

type (
	Content struct {
		Id               string    `json:"id"`
		MembershipTypeID int       `gorm:"index" json:"membership_type_id"`
		Title            string    `json:"title"`
		Description      string    `json:"description"`
		Position         int       `json:"position"`
		VideoURL         string    `json:"video_url"`
		CreatedAt        time.Time `json:"created_at"`
		UpdatedAt        time.Time `json:"updated_at"`
		Deleted          bool      `json:"deleted"`
	}

	ContentInput struct {
		MembershipTypeID int    `gorm:"index" json:"membership_type_id" binding:"required"`
		Title            string `json:"title" binding:"required"`
		Description      string `json:"description" binding:"required"`
		Position         int    `json:"position" binding:"required"`
		VideoURL         string `json:"video_url" binding:"required"`
	}

	ContentUpdateInput struct {
		MembershipTypeID int    `gorm:"index" json:"membership_type_id"`
		Title            string `json:"title"`
		Description      string `json:"description"`
		Position         int    `json:"position"`
		VideoURL         string `json:"video_url"`
	}
)
