package model

import "time"

type (
	UserDetail struct {
		Id          string    `json:"id"`
		UserID      int       `json:"user_id"`
		PlaceBirth  string    `json:"place_birth"`
		DateBirth   time.Time `json:"date_birth"`
		Gender      string    `json:"gender"`
		FullAddress string    `json:"full_address"`
		Agency      string    `json:"agency"`
	}

	UserDetailInput struct {
		PlaceBirth  string    `json:"place_birth" binding:"required"`
		DateBirth   time.Time `json:"date_birth" binding:"required"`
		Gender      string    `json:"gender" binding:"required"`
		FullAddress string    `json:"full_address" binding:"required"`
		Agency      string    `json:"agency"`
	}
)
