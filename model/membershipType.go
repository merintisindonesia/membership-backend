package model

import "time"

type (
	MembershipType struct {
		Id        int       `gorm:"primaryKey" json:"id"`
		Type      string    `json:"type"`
		Status    string    `json:"status"`
		CreatedAt time.Time `json:"created_at"`
		UpdatedAt time.Time `json:"updated_at"`
	}

	// get by id
	MembershipTypeAll struct {
		Id        int       `gorm:"primaryKey" json:"id"`
		Type      string    `json:"type"`
		Status    string    `json:"status"`
		CreatedAt time.Time `json:"created_at"`
		UpdatedAt time.Time `json:"updated_at"`
		Contents  []Content `json:"contents" gorm:"foreignKey:MembershipTypeID"`
		Exams     []Exam    `json:"exams" gorm:"foreignKey:MembershipTypeID"`
	}
)
