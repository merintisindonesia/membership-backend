package model

import "time"

type (
	Exam struct {
		Id               string    `gorm:"primaryKey" json:"id"`
		MembershipTypeID int       `gorm:"index" json:"membership_type_id"`
		Question         string    `json:"question"`
		Choice           string    `json:"choice"` // dipecah dengan ","
		CorrectAnswer    string    `json:"correct_answer"`
		CreatedAt        time.Time `json:"created_at"`
	}

	ExamInput struct {
		MembershipTypeID int      `gorm:"index" json:"membership_type_id" binding:"required"`
		Question         string   `json:"question" binding:"required"`
		Choice           []string `json:"choice" binding:"required"` // diisi array json
		CorrectAnswer    string   `json:"correct_answer" binding:"required"`
	}
	ExamInputUpdate struct {
		MembershipTypeID int      `gorm:"index" json:"membership_type_id"`
		Question         string   `json:"question"`
		Choice           []string `json:"choice"` // diisi array json
		CorrectAnswer    string   `json:"correct_answer"`
	}

	ExamOutput struct {
		Id               string    `gorm:"primaryKey" json:"id"`
		MembershipTypeID int       `gorm:"index" json:"membership_type_id"`
		Question         string    `json:"question"`
		Choice           []string  `json:"choice"` // dipecah dengan ","
		CorrectAnswer    string    `json:"correct_answer"`
		CreatedAt        time.Time `json:"created_at"`
	}
)
