package model

import "time"

type (
	MembershipExam struct {
		Id           string    `json:"id"`
		MembershipID string    `json:"membership_id"`
		Score        int       `json:"score"`
		CountGetExam int       `json:"count_get_exam"` // 2 if finish, 1 is not done
		CreatedAt    time.Time `json:"created_at"`
		TimeLength   time.Time `json:"time_length"` // max 1 jam
	}

	MembershipExamInput struct {
		Score        int `json:"score"`
		CountGetExam int `json:"count_get_exam" binding:"required"`
	}

	BulkDeleteInput struct {
		Id []string `json:"id"`
	}
)

// pr exam

// waktu diset dari awal ambil exam
// maksimal ambil 3 exam selama 1 minggu dari set awal ambil exam
// jika sudah 1 minggu baru bisa ambli exam lagi
