package service

import (
	"errors"
	"fmt"
	"log"
	"os"
	"strconv"
	"time"

	"github.com/google/uuid"
	"gitlab.com/merintisindonesia/membership-backend/auth"
	"gitlab.com/merintisindonesia/membership-backend/model"
	"gitlab.com/merintisindonesia/membership-backend/repository"
	"gitlab.com/merintisindonesia/membership-backend/utils"
	"golang.org/x/crypto/bcrypt"
)

var (
	// err         = godotenv.Load()
	devEmail    = os.Getenv("EMAIL_DEV")
	devPassword = os.Getenv("PASSWORD_DEV")
)

type (
	DataMembershipService interface {
		GenerateOrderPaymentId(Type string, Status string) string
		LoginWithUserAccount(userLoginInput model.UserLoginInput) (model.UserSuccessLoginResponse, error)
		CreateUserMembership(memberRegInput model.DataMembershipRegisterInput) (model.DataMembershipWithpayment, error)
		PaidMemberAndBulkContent(memberID string) (utils.SuccessPaidModel, error)
		GetMembershipProfile(membershipID string, userID int) (model.UserMembershipProfile, error)
		ProcessPaymentWithMidtrans(input model.PaymentNotificationInput) (utils.SuccessPaidModel, error)
	}

	dataMembershipService struct {
		authService           auth.AuthService
		userRepo              repository.UserRepository
		userDetailRepo        repository.UserDetailRepository
		dataMembershipRepo    repository.DataMembershipRepository
		membershipContentRepo repository.MembershipContentRepository
		contentRepo           repository.ContentRepository
		paymentRepo           repository.PaymentRepository
		midtransService       MidtransService
		emailService          EmailService
	}
)

func NewDataMembershipService(authService auth.AuthService, userRepo repository.UserRepository, dataMembershipRepo repository.DataMembershipRepository, userDetailRepo repository.UserDetailRepository, membershipContentRepo repository.MembershipContentRepository, contentRepo repository.ContentRepository, paymentRepo repository.PaymentRepository, midtransService MidtransService, emailService EmailService) *dataMembershipService {
	return &dataMembershipService{
		authService:           authService,
		userRepo:              userRepo,
		userDetailRepo:        userDetailRepo,
		dataMembershipRepo:    dataMembershipRepo,
		membershipContentRepo: membershipContentRepo,
		contentRepo:           contentRepo,
		paymentRepo:           paymentRepo,
		midtransService:       midtransService,
		emailService:          emailService,
	}
}

func (s *dataMembershipService) GenerateOrderPaymentId(Type string, Status string) string {
	count := s.paymentRepo.CountingRow()

	var formatOrderId string

	switch {
	case Type == "Perintis" && Status == "basic":
		formatOrderId += "PRTB"
		break
	case Type == "Perintis" && Status == "premium":
		formatOrderId += "PRTP"
		break
	case Type == "Pengembang" && Status == "basic":
		formatOrderId += "PGBB"
		break
	case Type == "Pengembang" && Status == "premium":
		formatOrderId += "PGBP"
		break
	}

	countStr := fmt.Sprintf("%d", count.Counting+1)

	switch {
	case len(countStr) == 1:
		formatOrderId += "0000" + countStr
	case len(countStr) == 2:
		formatOrderId += "000" + countStr
	case len(countStr) == 3:
		formatOrderId += "00" + countStr
	case len(countStr) == 4:
		formatOrderId += "0" + countStr
	case len(countStr) == 5:
		formatOrderId += countStr
	}

	return formatOrderId
}

func (s *dataMembershipService) CreateUserMembership(memberRegInput model.DataMembershipRegisterInput) (model.DataMembershipWithpayment, error) {
	// condition hanya create success
	// tapi masih belum cover jika pembayaran belum selesai
	// dicek dari user, jika ada data membership maka harus dibayar, atau jika ada maka harus diubah

	genID := uuid.New()
	dateNow := time.Now()
	userID := strconv.Itoa(memberRegInput.UserID)

	user, _ := s.userRepo.GetByID(userID)

	var newDataMember = model.DataMembership{
		Id:               genID.String(),
		UserID:           memberRegInput.UserID,
		Status:           memberRegInput.Status,
		Type:             memberRegInput.Type,
		ReasonJoinMember: memberRegInput.ReasonJoinMember,
		IsPaid:           false,
		CreatedAt:        dateNow,
		ExpiredDate:      dateNow.AddDate(1, 0, 0),
		Deleted:          false,
		CertificateName:  user.NmLengkap,
	}

	findingUserDetail, _ := s.userDetailRepo.FindByUserID(userID)

	if findingUserDetail.Id == "" || len(findingUserDetail.Id) <= 0 {
		var newUserDetail = model.UserDetail{
			Id:          genID.String(),
			UserID:      memberRegInput.UserID,
			PlaceBirth:  memberRegInput.PlaceBirth,
			DateBirth:   memberRegInput.DateBirth,
			Gender:      memberRegInput.Gender,
			FullAddress: memberRegInput.FullAddress,
			Agency:      memberRegInput.Agency,
		}

		_, err := s.userDetailRepo.Create(newUserDetail)

		if err != nil {
			log.Println("error create user detail ", err.Error())
			return model.DataMembershipWithpayment{}, err
		}
	}

	dataMembership, err := s.dataMembershipRepo.Create(newDataMember)

	// format order_id

	// [TYPE][STATUS][5 digit row]
	// PRT, PGB, B, P, 00001
	// ex : PRTP00001
	orderId := s.GenerateOrderPaymentId(dataMembership.Type, dataMembership.Status)

	var paymentTransaction = model.PaymentTransaction{
		ID:     dataMembership.Id,
		Amount: int(memberRegInput.Amount),
	}

	paymentURL, err := s.midtransService.MidtransPaymentURL(paymentTransaction, user, dataMembership.Id)

	if err != nil {
		return model.DataMembershipWithpayment{}, err
	}

	var newPayment = model.Payment{
		Id:            uuid.NewString(),
		MembershipID:  dataMembership.Id,
		OrderID:       orderId,
		PaymentMethod: memberRegInput.PaymentMethod,
		Amount:        memberRegInput.Amount,
		CreatedAt:     time.Now(),
		Status:        "Pending",
		PaymentURL:    paymentURL,
	}

	payment, err := s.paymentRepo.Create(newPayment)

	fmt.Println("success create payment")

	if err != nil {
		return model.DataMembershipWithpayment{}, err
	}

	var successRegister = model.DataMembershipWithpayment{
		Id:               dataMembership.Id,
		UserID:           dataMembership.UserID,
		Status:           dataMembership.Status,
		Type:             dataMembership.Type,
		ReasonJoinMember: dataMembership.ReasonJoinMember,
		IsPaid:           dataMembership.IsPaid,
		CreatedAt:        dataMembership.CreatedAt,
		ExpiredDate:      dataMembership.ExpiredDate,
		Deleted:          dataMembership.Deleted,
		CertificateName:  dataMembership.CertificateName,
		OrderID:          payment.OrderID,
		Amount:           payment.Amount,
		PaymentURL:       payment.PaymentURL,
	}

	return successRegister, nil
}

func (s *dataMembershipService) LoginWithUserAccount(userLoginInput model.UserLoginInput) (model.UserSuccessLoginResponse, error) {
	var userSuccess model.UserSuccessLoginResponse
	var userMemberToken string
	var errMessage string
	var err error

	userSuccess.SetAuth = "Authorization: <token>"

	if userLoginInput.Email == devEmail && userLoginInput.Password == devPassword {
		userMemberToken, err = s.authService.GenerateToken(9999, "dev-id-data-membership", "developer", "developer", "developer")

		userSuccess.Role = "developer"
		userSuccess.Token = userMemberToken

		return userSuccess, nil
	}

	user, err := s.userRepo.GetByEmail(userLoginInput.Email)

	if err != nil {
		return userSuccess, err
	}

	if user.Id == 0 || len(user.Email) <= 1 {
		errMessage = fmt.Sprintf("error user email %s not found", userLoginInput.Email)

		return userSuccess, errors.New(errMessage)
	}

	// compare hash password bcrypt
	err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(userLoginInput.Password))

	if err != nil {
		return userSuccess, err
	}

	userID := strconv.Itoa(user.Id)

	dataMembership, err := s.dataMembershipRepo.GetByUserID(userID)

	if err != nil {
		return userSuccess, err
	}

	if len(dataMembership.Id) <= 1 || len(dataMembership.Status) <= 1 {
		errMessage = fmt.Sprintf("error data membership with user email %s not found", userLoginInput.Email)

		return userSuccess, errors.New(errMessage)
	}

	if !dataMembership.IsPaid {
		errMessage = fmt.Sprintf("error data membership with user email %s in status payment verifications", userLoginInput.Email)

		return userSuccess, errors.New(errMessage)
	}

	userMemberToken, err = s.authService.GenerateToken(user.Id, dataMembership.Id, dataMembership.Type, dataMembership.Status, "user")

	if err != nil {
		return userSuccess, err
	}

	userSuccess.Role = "user"
	userSuccess.Token = userMemberToken

	return userSuccess, nil
}

// on progress
func (s *dataMembershipService) PaidMemberAndBulkContent(memberID string) (utils.SuccessPaidModel, error) {

	// step 1 get dataMember by memberID
	membership, err := s.dataMembershipRepo.GetByID(memberID)
	if err != nil {
		return utils.SuccessPaidModel{}, err
	}

	if membership.IsPaid == true {
		return utils.SuccessPaidModel{}, errors.New(fmt.Sprintf("membership id %s already activate", memberID))
	}

	payment, err := s.paymentRepo.FindByMemebershipId(membership.Id)

	if err != nil {
		log.Println("(Line 257) error get data payment by memberId :", err.Error())
		return utils.SuccessPaidModel{}, err
	}

	_, err = s.paymentRepo.UpdateById(payment.Id, map[string]interface{}{"status": "Accept"})

	if err != nil {
		log.Println("(Line 264) error process update after paid accept :", err.Error())
		return utils.SuccessPaidModel{}, err
	}

	// step 2 updating is paid to True if is paid false
	// =======================================
	var dataUpdate = map[string]interface{}{}

	dataUpdate["is_paid"] = true

	_, err = s.dataMembershipRepo.UpdateByID(memberID, dataUpdate)
	if err != nil {
		return utils.SuccessPaidModel{}, err
	}
	// ======================================

	// step 2.1 check type user membership
	var memberTypeID = "0"

	switch {
	case membership.Type == "Perintis" && membership.Status == "basic":
		memberTypeID = "1"
		break
	case membership.Type == "Perintis" && membership.Status == "premium":
		memberTypeID = "2"
		break
	case membership.Type == "Pengembang" && membership.Status == "basic":
		memberTypeID = "3"
		break
	case membership.Type == "Pengembang" && membership.Status == "premium":
		memberTypeID = "4"
		break
	}

	if memberTypeID == "0" {
		return utils.SuccessPaidModel{}, errors.New("something wrong with database membership id : data member service line 214")
	}
	// ================================================

	// step 3 : get contents bulk with parameter memberTypeID
	var contents = []model.Content{}

	if memberTypeID == "1" || memberTypeID == "3" {
		contents, _ = s.contentRepo.GetByMemberType(memberTypeID)
	} else if memberTypeID == "2" {
		contents, _ = s.contentRepo.GetByMemberTypePremium(memberTypeID, "1")
	} else if memberTypeID == "4" {
		contents, _ = s.contentRepo.GetByMemberTypePremium(memberTypeID, "3")
	}

	// step 3.1 bulk to membershipcontent using id
	for i := 0; i < len(contents); i++ {
		genID := uuid.New()
		newMemberContent := model.MembershipContent{
			Id:           genID.String(),
			MembershipID: membership.Id,
			ContentID:    contents[i].Id,
			Notes:        "",
			Done:         false,
			CreatedAt:    time.Now(),
			UpdatedAt:    time.Now(),
		}

		_, err := s.membershipContentRepo.Create(newMemberContent)

		if err != nil {
			return utils.SuccessPaidModel{}, err
		}
	}

	return *utils.SuccessPaidMembership(memberID), nil
}

func (s *dataMembershipService) GetMembershipProfile(membershipID string, userID int) (model.UserMembershipProfile, error) {
	userIDstr := strconv.Itoa(userID)

	// change in one query database ()
	userProfile, err := s.userRepo.GetProfileByID(userIDstr)

	// delete soon
	// dataMembership, err := s.dataMembershipRepo.GetByiD(membershipID)

	// delete soon
	// userDetail, err := s.userDetailRepo.FindByUserID(userIDstr)

	if err != nil {
		return userProfile, err
	}

	return userProfile, nil
}

func (s *dataMembershipService) ProcessPaymentWithMidtrans(input model.PaymentNotificationInput) (utils.SuccessPaidModel, error) {
	var dataUpdates = map[string]interface{}{}
	var checked bool = false

	if input.PaymentType == "credit_card" && input.TransactionStatus == "capture" && input.FraudStatus == "accept" {
		dataUpdates["status"] = "Accept"
		checked = true
	} else if input.TransactionStatus == "settlement" {
		dataUpdates["status"] = "Accept"
		checked = true
	} else if input.TransactionStatus == "deny" || input.TransactionStatus == "expired" || input.TransactionStatus == "cancel" {
		dataUpdates["status"] = "Reject"
		checked = false
	}
	// process paid with midtrans
	// order id sebagai parameter datamembershipID
	membership, err := s.dataMembershipRepo.GetByID(input.OrderID)
	if err != nil {
		return utils.SuccessPaidModel{}, err
	}

	if membership.IsPaid == true {
		return utils.SuccessPaidModel{}, errors.New(fmt.Sprintf("membership id %s already activate", input.OrderID))
	}

	userId := strconv.Itoa(membership.UserID)

	user, err := s.userRepo.GetByID(userId)

	payment, err := s.paymentRepo.FindByMemebershipId(membership.Id)

	if err != nil {
		log.Println("(Line 384) (mid) error get data payment by memberId :", err.Error())
		return utils.SuccessPaidModel{}, err
	}

	_, err = s.paymentRepo.UpdateById(payment.Id, dataUpdates)

	if err != nil {
		log.Println("(Line 391) (mid) error process update after paid accept :", err.Error())
		return utils.SuccessPaidModel{}, err
	}

	// step 2 updating is paid to True if is paid false
	// =======================================
	var dataUpdate = map[string]interface{}{}

	if checked {
		dataUpdate["is_paid"] = true
	} else {
		dataUpdate["is_paid"] = false
	}

	_, err = s.dataMembershipRepo.UpdateByID(membership.Id, dataUpdate)
	if err != nil {
		return utils.SuccessPaidModel{}, err
	}
	// ======================================

	// step 2.1 check type user membership
	var memberTypeID = "0"

	switch {
	case membership.Type == "Perintis" && membership.Status == "basic":
		memberTypeID = "1"
		break
	case membership.Type == "Perintis" && membership.Status == "premium":
		memberTypeID = "2"
		break
	case membership.Type == "Pengembang" && membership.Status == "basic":
		memberTypeID = "3"
		break
	case membership.Type == "Pengembang" && membership.Status == "premium":
		memberTypeID = "4"
		break
	}

	if memberTypeID == "0" {
		return utils.SuccessPaidModel{}, errors.New("something wrong with database membership id : data member service line 214")
	}
	// ================================================

	// step 3 : get contents bulk with parameter memberTypeID
	var contents = []model.Content{}

	if memberTypeID == "1" || memberTypeID == "3" {
		contents, _ = s.contentRepo.GetByMemberType(memberTypeID)
	} else if memberTypeID == "2" {
		contents, _ = s.contentRepo.GetByMemberTypePremium(memberTypeID, "1")
	} else if memberTypeID == "4" {
		contents, _ = s.contentRepo.GetByMemberTypePremium(memberTypeID, "3")
	}

	err = s.emailService.SendWelcomeEmailMembership(user.NmLengkap, membership.Type, user.Email)
	if err != nil {
		return utils.SuccessPaidModel{}, err
	}

	// step 3.1 bulk to membershipcontent using id
	for i := 0; i < len(contents); i++ {
		genID := uuid.New()
		newMemberContent := model.MembershipContent{
			Id:           genID.String(),
			MembershipID: membership.Id,
			ContentID:    contents[i].Id,
			Notes:        "",
			Done:         false,
			CreatedAt:    time.Now(),
			UpdatedAt:    time.Now(),
		}

		_, err := s.membershipContentRepo.Create(newMemberContent)

		if err != nil {
			return utils.SuccessPaidModel{}, err
		}
	}

	return *utils.SuccessPaidMembership(membership.Id), nil
}
