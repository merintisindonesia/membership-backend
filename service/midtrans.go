package service

import (
	"os"

	midtrans "github.com/veritrans/go-midtrans"
	"gitlab.com/merintisindonesia/membership-backend/model"
)

var (
	// err       = godotenv.Load()
	clientKey = os.Getenv("CLIENT_KEY_PAYMENT")
	serverKey = os.Getenv("SECRET_KEY_PAYMENT")
)

type MidtransService interface {
	MidtransPaymentURL(payment model.PaymentTransaction, user model.User, membershipId string) (string, error)
}

type midtransService struct {
}

func NewMidtransService() *midtransService {
	return &midtransService{}
}

func (s *midtransService) MidtransPaymentURL(payment model.PaymentTransaction, user model.User, membershipId string) (string, error) {

	midclient := midtrans.NewClient()
	midclient.ServerKey = serverKey
	midclient.ClientKey = clientKey
	midclient.APIEnvType = midtrans.Sandbox

	snapGateway := midtrans.SnapGateway{
		Client: midclient,
	}

	snapReq := &midtrans.SnapReq{
		CustomerDetail: &midtrans.CustDetail{
			Email: user.Email,
			FName: user.NmLengkap,
			Phone: user.NoHP,
		},
		TransactionDetails: midtrans.TransactionDetails{
			OrderID:  payment.ID,
			GrossAmt: int64(payment.Amount),
		},
	}

	snapTokenResp, err := snapGateway.GetToken(snapReq)

	if err != nil {
		return "", err
	}

	return snapTokenResp.RedirectURL, nil
}
