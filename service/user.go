package service

import (
	"errors"
	"time"

	"gitlab.com/merintisindonesia/membership-backend/model"
	"gitlab.com/merintisindonesia/membership-backend/repository"
	"golang.org/x/crypto/bcrypt"
)

type (
	UserService interface {
		CreateUser(input model.UserRegisterInput) (model.UserRegisterResponse, error)
		LoginUser(input model.UserLoginInput) (model.UserCheckLoginResponse, error)
		CheckMembershipByUserID(userId string) (model.UserMembershipProfile, error)
	}

	userService struct {
		userRepo           repository.UserRepository
		userDetailRepo     repository.UserDetailRepository
		dataMembershipRepo repository.DataMembershipRepository
		paymentRepo        repository.PaymentRepository
		midtrans           midtransService
	}
)

func NewUserService(userRepo repository.UserRepository, dataMembershipRepo repository.DataMembershipRepository, userDetailRepo repository.UserDetailRepository, paymentRepo repository.PaymentRepository, midtrans midtransService) *userService {
	return &userService{userRepo: userRepo, dataMembershipRepo: dataMembershipRepo, userDetailRepo: userDetailRepo, paymentRepo: paymentRepo, midtrans: midtrans}
}

func (s *userService) CreateUser(input model.UserRegisterInput) (model.UserRegisterResponse, error) {

	emailCheck, _ := s.userRepo.GetByEmail(input.Email)

	if len(emailCheck.Email) > 0 || emailCheck.Email != "" {
		return model.UserRegisterResponse{}, errors.New("user email already registered")
	}

	hashPass, err := bcrypt.GenerateFromPassword([]byte(input.Password), 11)

	if err != nil {
		return model.UserRegisterResponse{}, err
	}

	var newUser = model.User{
		Id:            input.Id,
		NmLengkap:     input.NmLengkap,
		NoHP:          input.NoHP,
		Email:         input.Email,
		Password:      string(hashPass),
		RememberToken: "",
	}

	user, err := s.userRepo.Create(newUser)

	if err != nil {
		return model.UserRegisterResponse{}, err
	}

	var userOutput = model.UserRegisterResponse{
		Id:        user.Id,
		NmLengkap: user.NmLengkap,
		NoHP:      user.NoHP,
		Email:     user.Email,
	}

	return userOutput, nil
}

func (r *userService) LoginUser(input model.UserLoginInput) (model.UserCheckLoginResponse, error) {
	user, err := r.userRepo.GetByEmail(input.Email)

	if err != nil {
		return model.UserCheckLoginResponse{}, err
	}

	if user.Id == 0 || user.Email == "" {
		return model.UserCheckLoginResponse{}, errors.New("invalid email / password od user not registered")
	}

	err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(input.Password))
	if err != nil {
		return model.UserCheckLoginResponse{}, err
	}

	var userOutput = model.UserCheckLoginResponse{
		Id:    user.Id,
		Email: user.Email,
	}

	return userOutput, nil
}

func (s *userService) CheckMembershipByUserID(userId string) (model.UserMembershipProfile, error) {
	user, err := s.userRepo.GetByID(userId)

	userDetail, err := s.userDetailRepo.FindByUserID(userId)

	dataMembership, err := s.dataMembershipRepo.GetByUserID(userId)

	var payment model.Payment

	if dataMembership.Id != "" {
		payment, err = s.paymentRepo.FindByMemebershipId(dataMembership.Id)
	}

	var userProfile = model.UserMembershipProfile{
		Id:               user.Id,
		NmLengkap:        user.NmLengkap,
		Email:            user.Email,
		NoHP:             user.NoHP,
		Type:             dataMembership.Type,
		Status:           dataMembership.Status,
		ReasonJoinMember: dataMembership.ReasonJoinMember,
		IsPaid:           dataMembership.IsPaid,
		CreatedAt:        dataMembership.CreatedAt,
		ExpiredDate:      dataMembership.ExpiredDate,
		CertificateName:  dataMembership.CertificateName,
		PlaceBirth:       userDetail.PlaceBirth,
		DateBirth:        userDetail.DateBirth,
		Gender:           userDetail.Gender,
		FullAddress:      userDetail.FullAddress,
		Agency:           userDetail.Agency,
	}

	if payment.Id == "" {
		userProfile.PaymentURL = ""
	} else if payment.Id != "" && time.Now().After(payment.CreatedAt.Add(time.Duration(24)*time.Hour)) {
		newUrl, err := s.midtrans.MidtransPaymentURL(model.PaymentTransaction{
			ID:     dataMembership.Id,
			Amount: int(payment.Amount),
		}, user, dataMembership.Id)

		if err != nil {
			return model.UserMembershipProfile{}, err
		}

		updatedPayment, err := s.paymentRepo.UpdateById(payment.Id, map[string]interface{}{
			"payment_url": newUrl,
			"amount":      payment.Amount,
		})

		if err != nil {
			return model.UserMembershipProfile{}, err
		}

		userProfile.PaymentURL = updatedPayment.PaymentURL
	} else {
		userProfile.PaymentURL = payment.PaymentURL
	}

	if err != nil {
		return userProfile, err
	}

	return userProfile, nil
}
