package service

import (
	"github.com/google/uuid"
	"gitlab.com/merintisindonesia/membership-backend/model"
	"gitlab.com/merintisindonesia/membership-backend/repository"
)

type (
	TalkShowVideoService interface {
		GetAll() ([]model.TalkshowVideo, error)
		CreateNewTalkshow(input model.TalkshowVideoInput) (model.TalkshowVideo, error)
		BulkCreateTalkshow(inputs []model.TalkshowVideoInput) ([]model.TalkshowVideo, error)
	}

	talkshowVideoService struct {
		talkshowRepo repository.TalkshowVideoRepository
	}
)

func NewTalkshowVideoService(talkshowRepo repository.TalkshowVideoRepository) *talkshowVideoService {
	return &talkshowVideoService{talkshowRepo: talkshowRepo}
}

func (s *talkshowVideoService) GetAll() ([]model.TalkshowVideo, error) {
	talkshows, err := s.talkshowRepo.GetAll()

	if err != nil {
		return talkshows, err
	}

	return talkshows, nil
}

func (s *talkshowVideoService) CreateNewTalkshow(input model.TalkshowVideoInput) (model.TalkshowVideo, error) {
	newID := uuid.New()

	var newTalkshow = model.TalkshowVideo{
		Id:          newID.String(),
		Title:       input.Title,
		Description: input.Description,
		VideoURL:    input.VideoURL,
		ImageURL:    input.ImageURL,
	}
	talkshow, err := s.talkshowRepo.Create(newTalkshow)

	if err != nil {
		return talkshow, err
	}

	return talkshow, nil
}

func (s *talkshowVideoService) BulkCreateTalkshow(inputs []model.TalkshowVideoInput) ([]model.TalkshowVideo, error) {
	var talkshows []model.TalkshowVideo

	for i := 0; i < len(inputs); i++ {
		newTalkshow, err := s.CreateNewTalkshow(inputs[i])
		if err != nil {
			return talkshows, err
		}

		talkshows = append(talkshows, newTalkshow)
	}

	return talkshows, nil
}
