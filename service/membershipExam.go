package service

import (
	"fmt"
	"sync"
	"time"

	"github.com/google/uuid"
	"gitlab.com/merintisindonesia/membership-backend/model"
	"gitlab.com/merintisindonesia/membership-backend/repository"
	"gitlab.com/merintisindonesia/membership-backend/utils"
)

type (
	MembershipExamService interface {
		GetAllByMemberID(memberID string) ([]model.MembershipExam, error)
		GetMemberExamById(memberExamId string) (model.MembershipExam, error)
		CreateNewMemberExam(memberID string) (model.MembershipExam, error)
		UpdateForDone(input model.MembershipExamInput, memberExamID string) (model.MembershipExam, error)
		DeleteMembershipExamById(id string) (*utils.DeleteMessageModel, error)
		BulkDeleteMembershipExam(input model.BulkDeleteInput) ([]*utils.DeleteMessageModel, error)
	}

	membershipExamService struct {
		membershipExamRepo      repository.MembershipExamRepository
		membershipQuestExamRepo repository.MembershipQuestExamRepo
		examService             ExamService
		dataMembershipRepo      repository.DataMembershipRepository
	}
)

func NewMembershipExamService(membershipExamRepo repository.MembershipExamRepository, membershipQuestExamRepo repository.MembershipQuestExamRepo, examService ExamService, dataMembershipRepo repository.DataMembershipRepository) *membershipExamService {
	return &membershipExamService{
		membershipExamRepo:      membershipExamRepo,
		membershipQuestExamRepo: membershipQuestExamRepo,
		examService:             examService,
		dataMembershipRepo:      dataMembershipRepo,
	}
}

func (s *membershipExamService) GetAllByMemberID(memberID string) ([]model.MembershipExam, error) {
	memberExams, err := s.membershipExamRepo.GetAllByMemberID(memberID)

	if err != nil {
		return memberExams, err
	}

	return memberExams, nil
}

func (s *membershipExamService) GetMemberExamById(memberExamId string) (model.MembershipExam, error) {
	memberExam, err := s.membershipExamRepo.GetById(memberExamId)

	if err != nil {
		return memberExam, err
	}

	return memberExam, nil
}

func (s *membershipExamService) CreateNewMemberExam(memberID string) (model.MembershipExam, error) {

	genID := uuid.New()
	dateNow := time.Now()

	var newMemberExam = model.MembershipExam{
		Id:           genID.String(),
		MembershipID: memberID,
		Score:        0,
		CountGetExam: 1,
		CreatedAt:    dateNow,
		TimeLength:   dateNow.Add(time.Minute * time.Duration(30)),
	}

	memberExam, err := s.membershipExamRepo.Create(newMemberExam)

	// create question exams insert using exam shuffle, and insert to member question exam table
	// 1 get member type from member id

	// fmt.Println("line 67 service membershipExam")
	// fmt.Println(memberExam)

	membership, err := s.dataMembershipRepo.GetByID(memberID)

	if err != nil {
		return memberExam, err
	}

	// fmt.Println("line 76 service membershipExam")
	// data, _ := json.Marshal(membership)

	// fmt.Println(string(data))

	examShuffle, err := s.examService.ShowShuffleExam(membership.Type, membership.Status)

	if err != nil {
		return memberExam, err
	}

	// fmt.Println("line 85 service membershipExam")
	// fmt.Println(examShuffle)

	// add waitgroup variable
	var wg sync.WaitGroup

	for _, exam := range examShuffle {
		wg.Add(1)

		go func(ex model.ExamOutput) {
			defer wg.Done()

			errs := s.membershipQuestExamRepo.CreateQuestioExam(model.MembershipQuestionExam{
				Id:               uuid.NewString(),
				MembershipExamId: memberExam.Id,
				Question:         ex.Question,
				Choice:           utils.JoinArrToString(ex.Choice),
				CorrectAnswer:    ex.CorrectAnswer,
				Answer:           "",
			})

			if err != nil {
				err = errs
			}
		}(exam)
	}

	wg.Wait()

	if err != nil {
		return memberExam, err
	}

	return memberExam, nil
}

func (s *membershipExamService) UpdateForDone(input model.MembershipExamInput, memberExamID string) (model.MembershipExam, error) {
	var dataUpdates = map[string]interface{}{}

	if input.Score > 0 {
		dataUpdates["score"] = input.Score
	}
	dataUpdates["count_get_exam"] = 2

	memberExam, err := s.membershipExamRepo.UpdateByMemberExamID(memberExamID, dataUpdates)

	if err != nil {
		return memberExam, err
	}

	return memberExam, nil
}

func (s *membershipExamService) DeleteMembershipExamById(id string) (*utils.DeleteMessageModel, error) {

	var deleteMessage *utils.DeleteMessageModel

	err := s.membershipQuestExamRepo.DeleteByMemberExamId(id)

	if err != nil {
		return deleteMessage, err
	}

	err = s.membershipExamRepo.DeleteById(id)

	if err != nil {
		return deleteMessage, err
	}

	deleteMessage = utils.DeleteMessage(fmt.Sprintf("success deleted membership exam id : %s", id), utils.Delete)

	return deleteMessage, nil
}

func (s *membershipExamService) BulkDeleteMembershipExam(input model.BulkDeleteInput) ([]*utils.DeleteMessageModel, error) {

	var deleteMessages []*utils.DeleteMessageModel

	for _, id := range input.Id {
		deleted, err := s.DeleteMembershipExamById(id)

		if err != nil {
			return deleteMessages, err
		}

		deleteMessages = append(deleteMessages, deleted)
	}

	return deleteMessages, nil
}
