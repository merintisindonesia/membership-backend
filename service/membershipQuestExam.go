package service

import (
	"gitlab.com/merintisindonesia/membership-backend/model"
	"gitlab.com/merintisindonesia/membership-backend/repository"
)

type (
	MemberQuestExamService interface {
		GetAllByMemberExamId(memberExamId string) ([]model.MembershipQuestionExamOutput, error)
		UpdateAnswerByQuestionExamId(questExamId string, input model.MembershipQuestExamInputAnswer) (model.MembershipQuestionExam, error)
	}

	memberQuestExamService struct {
		memberQuestExamRepo repository.MembershipQuestExamRepo
	}
)

func NewMembershipQuestExamService(memberQuestExamRepo repository.MembershipQuestExamRepo) *memberQuestExamService {
	return &memberQuestExamService{memberQuestExamRepo: memberQuestExamRepo}
}

func (s *memberQuestExamService) GetAllByMemberExamId(memberExamId string) ([]model.MembershipQuestionExamOutput, error) {
	membershipQuestExams, err := s.memberQuestExamRepo.GetQuestionByMemberExamId(memberExamId)

	if err != nil {
		return []model.MembershipQuestionExamOutput{}, err
	}

	var outputMemberQuestExams []model.MembershipQuestionExamOutput

	for _, memberQuestExam := range membershipQuestExams {
		outputMemberQuestExams = append(outputMemberQuestExams, *memberQuestExam.ToOutput())
	}

	return outputMemberQuestExams, nil
}

func (s *memberQuestExamService) UpdateAnswerByQuestionExamId(questExamId string, input model.MembershipQuestExamInputAnswer) (model.MembershipQuestionExam, error) {
	var dataUpdates = map[string]interface{}{}

	dataUpdates["answer"] = input.Answer

	memberQuestExam, err := s.memberQuestExamRepo.UpdateMembershipExamById(questExamId, dataUpdates)
	if err != nil {
		return memberQuestExam, err
	}

	return memberQuestExam, nil
}
