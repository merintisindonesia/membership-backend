package service

import (
	"fmt"
	"time"

	"github.com/google/uuid"
	"gitlab.com/merintisindonesia/membership-backend/model"
	"gitlab.com/merintisindonesia/membership-backend/repository"
	"gitlab.com/merintisindonesia/membership-backend/utils"
)

type (
	ContentService interface {
		ShowAll() ([]model.Content, error)
		ShowByID(id string) (model.Content, error)
		ShowByMemberTypeID(memberTypeID string) ([]model.Content, error)
		CreateContent(input model.ContentInput) (model.Content, error)
		BulkCreateContent(inputs []model.ContentInput) ([]model.Content, error)
		UpdateByID(id string, input model.ContentUpdateInput) (model.Content, error)
		DeleteByID(id string) (utils.DeleteMessageModel, error)
	}

	contentService struct {
		contentRepo repository.ContentRepository
	}
)

func NewContentService(contentRepo repository.ContentRepository) *contentService {
	return &contentService{contentRepo: contentRepo}
}

func (s *contentService) ShowAll() ([]model.Content, error) {
	contents, err := s.contentRepo.GetAll()

	if err != nil {
		return contents, err
	}

	return contents, nil
}
func (s *contentService) ShowByID(id string) (model.Content, error) {
	content, err := s.contentRepo.GetByID(id)

	if err != nil {
		return content, err
	}

	return content, nil
}
func (s *contentService) ShowByMemberTypeID(memberTypeID string) ([]model.Content, error) {
	contents, err := s.contentRepo.GetByMemberType(memberTypeID)

	if err != nil {
		return contents, err
	}

	return contents, nil
}

func (s *contentService) BulkCreateContent(inputs []model.ContentInput) ([]model.Content, error) {
	var contents []model.Content

	for i := 0; i < len(inputs); i++ {
		newContent, err := s.CreateContent(inputs[i])
		if err != nil {
			return contents, err
		}

		contents = append(contents, newContent)
	}

	return contents, nil
}

func (s *contentService) CreateContent(input model.ContentInput) (model.Content, error) {

	generateId := uuid.New()

	var newContent = model.Content{
		Id:               generateId.String(),
		MembershipTypeID: input.MembershipTypeID,
		Title:            input.Title,
		Description:      input.Description,
		Position:         input.Position,
		VideoURL:         input.VideoURL,
		CreatedAt:        time.Now(),
		UpdatedAt:        time.Now(),
		Deleted:          false,
	}

	content, err := s.contentRepo.Create(newContent)

	if err != nil {
		return content, err
	}

	return content, nil

}
func (s *contentService) UpdateByID(id string, input model.ContentUpdateInput) (model.Content, error) {
	var dataUpdates = map[string]interface{}{}

	if input.Title != "" {
		dataUpdates["title"] = input.Title
	}

	if input.Description != "" {
		dataUpdates["title"] = input.Description
	}

	if input.VideoURL != "" {
		dataUpdates["title"] = input.VideoURL
	}

	if input.MembershipTypeID != 0 {
		dataUpdates["membership_type_id"] = input.MembershipTypeID
	}

	if input.Position != 0 {
		dataUpdates["position"] = input.Position
	}

	dataUpdates["updated_at"] = time.Now()

	content, err := s.contentRepo.UpdateByID(id, dataUpdates)

	if err != nil {
		return content, err
	}

	return content, nil
}
func (s *contentService) DeleteByID(id string) (utils.DeleteMessageModel, error) {
	var dataUpdates = map[string]interface{}{}

	dataUpdates["deleted"] = true
	dataUpdates["updated_at"] = time.Now()

	_, err := s.contentRepo.UpdateByID(id, dataUpdates)

	if err != nil {
		return utils.DeleteMessageModel{}, err
	}

	return *utils.DeleteMessage(fmt.Sprintf("success deleted content id %s", id), utils.SoftDelete), nil

}
