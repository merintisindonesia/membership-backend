package service

import (
	"time"

	"gitlab.com/merintisindonesia/membership-backend/model"
	"gitlab.com/merintisindonesia/membership-backend/repository"
)

type (
	MembershipContentService interface {
		ShowAllByMembershipID(membershipID string) ([]model.MembershipContentWithContent, error)
		UpdateNotesByContentID(id string, notes string) (model.MembershipContentWithContent, error)
		UpdateDoneByContentID(id string) (model.MembershipContentWithContent, error)
		GetMembershipContentByID(id string) (model.MembershipContentWithContent, error)
	}

	membershipContentService struct {
		membershipContentRepo repository.MembershipContentRepository
		contentRepo           repository.ContentRepository
	}
)

func NewMembershipContentService(membershipContentRepo repository.MembershipContentRepository, contentRepo repository.ContentRepository) *membershipContentService {
	return &membershipContentService{membershipContentRepo: membershipContentRepo, contentRepo: contentRepo}
}

func (s *membershipContentService) ShowAllByMembershipID(membershipID string) ([]model.MembershipContentWithContent, error) {
	membershipContents, err := s.membershipContentRepo.GetAllByMembershipIDWIthCOntent(membershipID)

	if err != nil {
		return membershipContents, err
	}

	return membershipContents, nil
}

func (s *membershipContentService) UpdateNotesByContentID(id string, notes string) (model.MembershipContentWithContent, error) {
	dataUpdate := map[string]interface{}{}

	dataUpdate["notes"] = notes
	dataUpdate["updated_at"] = time.Now()

	membershipContent, err := s.membershipContentRepo.UpdateByID(id, dataUpdate)

	if err != nil {
		return membershipContent, err
	}

	return membershipContent, nil
}

func (s *membershipContentService) UpdateDoneByContentID(id string) (model.MembershipContentWithContent, error) {
	dataUpdate := map[string]interface{}{}

	dataUpdate["done"] = true
	dataUpdate["updated_at"] = time.Now()

	membershipContent, err := s.membershipContentRepo.UpdateByID(id, dataUpdate)

	if err != nil {
		return membershipContent, err
	}

	return membershipContent, nil
}

func (s *membershipContentService) GetMembershipContentByID(id string) (model.MembershipContentWithContent, error) {
	memberContent, err := s.membershipContentRepo.GetMembershipContentByID(id)

	if err != nil {
		return memberContent, err
	}

	return memberContent, nil
}
