package service

import (
	"fmt"
	"log"
	"os"

	"gitlab.com/merintisindonesia/membership-backend/model"
	"gopkg.in/gomail.v2"
)

var (
	// err                  = godotenv.Load()
	emailName            = os.Getenv("EMAIL_NAME")
	emailPass            = os.Getenv("EMAIL_PASS")
	CONFIG_SMTP_HOST     = "smtp.gmail.com"
	CONFIG_SMTP_PORT     = 587
	CONFIG_SENDER_NAME   = fmt.Sprintf("MERINTIS INDONESIA <%s>", emailName)
	CONFIG_AUTH_EMAIL    = emailName
	CONFIG_AUTH_PASSWORD = emailPass
)

type (
	EmailService interface {
		SendWelcomeEmailMembership(name string, membershipType string, email string) error
	}

	emailService struct {
	}
)

func NewEmailService() *emailService {
	return &emailService{}
}

func (s *emailService) SendWelcomeEmailMembership(name string, membershipType string, email string) error {
	sendBody := model.SendBodyWelcome(name, membershipType)

	mailer := gomail.NewMessage()
	mailer.SetHeader("From", CONFIG_SENDER_NAME)
	mailer.SetHeader("To", email)
	mailer.SetAddressHeader("Cc", "info.merintisindonesia@gmail.com", "membership - merintis indonesia")
	mailer.SetHeader("Subject", "Welcome to Membership - Merintis Indonesia")
	mailer.SetBody("text/html", sendBody)
	// mailer.Attach("./sample.png")

	dialer := gomail.NewDialer(
		CONFIG_SMTP_HOST,
		CONFIG_SMTP_PORT,
		CONFIG_AUTH_EMAIL,
		CONFIG_AUTH_PASSWORD,
	)

	err := dialer.DialAndSend(mailer)
	if err != nil {
		log.Println("error send email", err.Error())
		return err
	}

	log.Println("Mail sent!")

	return nil
}
