package service

import (
	"errors"

	"github.com/google/uuid"
	"gitlab.com/merintisindonesia/membership-backend/model"
	"gitlab.com/merintisindonesia/membership-backend/repository"
)

type (
	RefferalService interface {
		GetRefferalByCode(code string) (model.RefferalResponse, error)
		UseRefferalByCode(code string) (model.RefferalSuccessUsedResponse, error)

		GetAllRefferal() ([]model.Refferal, error)
		CreateNewRefferal(input model.RefferalInput) (model.Refferal, error)
		UpdateRefferalById(id string, input model.RefferalUpdateInput) (model.Refferal, error)
		InactiveRefferalById(id string) (model.Refferal, error)
	}

	refferalService struct {
		refferalRepo repository.RefferalRepository
	}
)

func NewRefferalService(refferalRepo repository.RefferalRepository) *refferalService {
	return &refferalService{refferalRepo: refferalRepo}
}

func (s *refferalService) GetRefferalByCode(code string) (model.RefferalResponse, error) {

	refferal, err := s.refferalRepo.GetByCode(code)

	if err != nil {
		return model.RefferalResponse{}, err
	}

	if refferal.ActiveCode == false {
		return model.RefferalResponse{}, errors.New("status code refferal inactive")
	}

	if refferal.Limit >= refferal.UsedCode {
		return model.RefferalResponse{}, errors.New("code refferal has been used the maximum limit")
	}

	return model.RefferalResponse{
		Status: "active",
		Details: model.RefferalDetailResponse{
			Code:     refferal.Code,
			Discount: refferal.Discount,
			Message:  "you can use the refferal code",
		},
	}, nil
}
func (s *refferalService) UseRefferalByCode(code string) (model.RefferalSuccessUsedResponse, error) {
	refferal, err := s.refferalRepo.GetByCode(code)

	if err != nil {
		return model.RefferalSuccessUsedResponse{}, err
	}

	if refferal.Id == "" {
		return model.RefferalSuccessUsedResponse{}, errors.New("error code refferal not found")
	}

	var dataUpdate = map[string]interface{}{}
	dataUpdate["used_code"] = refferal.UsedCode + 1

	refferalUse, err := s.refferalRepo.UpdateByCode(code, dataUpdate)

	if err != nil {
		return model.RefferalSuccessUsedResponse{}, err
	}

	return model.RefferalSuccessUsedResponse{
		Status:  "success",
		Code:    refferalUse.Code,
		Message: "code refferal success to used",
	}, nil

}

func (s *refferalService) GetAllRefferal() ([]model.Refferal, error) {

	refferals, err := s.refferalRepo.GetAll()

	if err != nil {
		return refferals, err
	}

	return refferals, nil
}
func (s *refferalService) CreateNewRefferal(input model.RefferalInput) (model.Refferal, error) {

	refferal, err := s.refferalRepo.GetByCode(input.Code)

	if err != nil {
		return refferal, err
	}

	if refferal.Id != "" || refferal.Code != "" {
		return refferal, errors.New("code refferais has been create in database, please create unique code")
	}

	refId := uuid.New()

	newRefferal := model.Refferal{
		Id:         refId.String(),
		Code:       input.Code,
		Discount:   input.Discount,
		Limit:      input.Limit,
		UsedCode:   0,
		ActiveCode: true,
	}

	createdRef, err := s.refferalRepo.Create(newRefferal)

	if err != nil {
		return createdRef, err
	}

	return createdRef, nil

}
func (s *refferalService) UpdateRefferalById(id string, input model.RefferalUpdateInput) (model.Refferal, error) {
	refferal, err := s.refferalRepo.GetByCode(input.Code)

	if err != nil {
		return refferal, err
	}

	if refferal.Id != "" || refferal.Code != "" {
		return refferal, errors.New("code refferais has been create in database, please create unique code")
	}

	var dataUpdates = map[string]interface{}{}

	if input.Code != "" {
		dataUpdates["code"] = input.Code
	}

	if input.Discount != 0 {
		dataUpdates["discount"] = input.Discount
	}

	if input.Limit != 0 {
		dataUpdates["limit"] = input.Limit
	}

	refferalUpdate, err := s.refferalRepo.UpdateById(id, dataUpdates)

	if err != nil {
		return refferalUpdate, err
	}

	return refferalUpdate, nil

}

func (s *refferalService) InactiveRefferalById(id string) (model.Refferal, error) {
	var dataUpdate = map[string]interface{}{}

	dataUpdate["active_code"] = false

	refferal, err := s.refferalRepo.UpdateById(id, dataUpdate)

	if err != nil {
		return refferal, err
	}

	return refferal, nil
}
