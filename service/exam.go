package service

import (
	"fmt"
	"math/rand"
	"time"

	"github.com/google/uuid"
	"gitlab.com/merintisindonesia/membership-backend/model"
	"gitlab.com/merintisindonesia/membership-backend/repository"
	"gitlab.com/merintisindonesia/membership-backend/utils"
)

type (
	ExamService interface {
		ShuffleQuestion(questions []model.Exam) []model.ExamOutput
		ShuffleChoice(choices []string) []string

		ChangeExamToExamOutput(value model.Exam) model.ExamOutput
		BulkChangeExamToExamOutput(vals []model.Exam) []model.ExamOutput

		ShowAll() ([]model.ExamOutput, error)
		ShowShuffleExam(memberType, memberStatus string) ([]model.ExamOutput, error)
		CreateNewExam(input model.ExamInput) (model.ExamOutput, error)

		BulkCreateNewExam(inputs []model.ExamInput) ([]model.ExamOutput, error)

		// UpdateExamByID(id string, input model.ExamInput) (model.ExamOutput, error)
		DeleteExamByID(id string) (utils.DeleteMessageModel, error)
	}

	examService struct {
		examRepo repository.ExamRepository
	}
)

func NewExamService(examRepo repository.ExamRepository) *examService {
	return &examService{examRepo: examRepo}
}

// service shuffle questions
func (s *examService) ShuffleQuestion(questions []model.Exam) []model.ExamOutput {
	var exams []model.ExamOutput

	for i := 0; i < len(questions); i++ {
		var examOutput = s.ChangeExamToExamOutput(questions[i])
		examOutput.Choice = s.ShuffleChoice(examOutput.Choice)
		exams = append(exams, examOutput)
	}

	r := rand.New(rand.NewSource(time.Now().Unix()))
	result := make([]model.ExamOutput, len(exams))
	n := len(exams)
	for i := 0; i < n; i++ {
		randIndex := r.Intn(len(exams))
		result[i] = exams[randIndex]
		exams = append(exams[:randIndex], exams[randIndex+1:]...)
	}
	return result[:15]
}

func (s *examService) ShuffleChoice(choices []string) []string {
	// r := rand.New(rand.NewSource(time.Now().Unix()))
	// result := make([]string, len(choices))
	// n := len(choices)
	// for i := 0; i < n; i++ {
	// 	randIndex := r.Intn(len(choices))
	// 	result[i] = choices[randIndex]
	// 	choices = append(choices[:randIndex], choices[randIndex+1:]...)
	// }
	// return choices

	rand.Seed(time.Now().UnixNano())
	for i := len(choices) - 1; i > 0; i-- { // Fisher–Yates shuffle
		j := rand.Intn(i + 1)
		choices[i], choices[j] = choices[j], choices[i]
	}

	return choices
}

func (s *examService) ChangeExamToExamOutput(value model.Exam) model.ExamOutput {
	var examOutput = model.ExamOutput{
		Id:               value.Id,
		MembershipTypeID: value.MembershipTypeID,
		Question:         value.Question,
		Choice:           utils.SplitStrToArrayStr(value.Choice),
		CorrectAnswer:    value.CorrectAnswer,
		CreatedAt:        value.CreatedAt,
	}

	return examOutput
}

func (s *examService) BulkChangeExamToExamOutput(vals []model.Exam) []model.ExamOutput {
	var examOutputs []model.ExamOutput

	for i := 0; i < len(vals); i++ {
		examOutput := s.ChangeExamToExamOutput(vals[i])
		examOutputs = append(examOutputs, examOutput)
	}

	return examOutputs
}

func (s *examService) ShowAll() ([]model.ExamOutput, error) {
	exams, err := s.examRepo.GetAll()

	if err != nil {
		return []model.ExamOutput{}, err
	}

	return s.BulkChangeExamToExamOutput(exams), nil
}

func (s *examService) ShowShuffleExam(memberType, memberStatus string) ([]model.ExamOutput, error) {
	var memberTypeID string

	switch {
	case memberType == "Perintis" && memberStatus == "basic":
		memberTypeID = "1"
		break
	case memberType == "Perintis" && memberStatus == "premium":
		memberTypeID = "2"
		break
	case memberType == "Pengembang" && memberStatus == "basic":
		memberTypeID = "3"
		break
	case memberType == "Pengembang" && memberStatus == "premium":
		memberTypeID = "4"
		break
	case memberType == "developer" && memberStatus == "developer":
		memberTypeID = "1"
		break
	}

	fmt.Println(memberType, memberStatus)
	fmt.Println(memberTypeID, "line 137 , exam service")

	exams, err := s.examRepo.GetAllByMemberType(memberTypeID)

	if err != nil {
		return []model.ExamOutput{}, err
	}

	shuffleExams := s.ShuffleQuestion(exams)

	return shuffleExams, nil
}

func (s *examService) BulkCreateNewExam(inputs []model.ExamInput) ([]model.ExamOutput, error) {
	var outputs []model.ExamOutput

	for i := 0; i < len(inputs); i++ {
		exam, err := s.CreateNewExam(inputs[i])

		if err != nil {
			return outputs, err
		}

		outputs = append(outputs, exam)
	}

	return outputs, nil
}

func (s *examService) CreateNewExam(input model.ExamInput) (model.ExamOutput, error) {
	newID := uuid.New()

	var examDB = model.Exam{
		Id:               newID.String(),
		MembershipTypeID: input.MembershipTypeID,
		Question:         input.Question,
		Choice:           utils.JoinArrToString(input.Choice),
		CorrectAnswer:    input.CorrectAnswer,
		CreatedAt:        time.Now(),
	}

	exam, err := s.examRepo.Create(examDB)

	if err != nil {
		return model.ExamOutput{}, err
	}

	return s.ChangeExamToExamOutput(exam), nil
}

func (s *examService) DeleteExamByID(id string) (utils.DeleteMessageModel, error) {
	deleted, err := s.examRepo.DeleteByID(id)

	if err != nil {
		return deleted, err
	}

	return deleted, nil
}
