package service

import (
	"errors"

	"gitlab.com/merintisindonesia/membership-backend/model"
	"gitlab.com/merintisindonesia/membership-backend/repository"
)

type (
	MembershipTypeService interface {
		ShowAll() ([]model.MembershipType, error)
		ShowAllByID(id string) (model.MembershipTypeAll, error)
	}

	membershipTypeService struct {
		membershipTypeRepo repository.MembershipTypeRepository
	}
)

func NewMembershipTypeService(membershipTypeRepo repository.MembershipTypeRepository) *membershipTypeService {
	return &membershipTypeService{membershipTypeRepo: membershipTypeRepo}
}

func (s *membershipTypeService) ShowAll() ([]model.MembershipType, error) {
	membershipTypes, err := s.membershipTypeRepo.GetAll()

	if err != nil {
		return membershipTypes, err
	}

	return membershipTypes, nil
}

func (s *membershipTypeService) ShowAllByID(id string) (model.MembershipTypeAll, error) {
	membershipType, err := s.membershipTypeRepo.GetByID(id)

	if membershipType.Id == 0 || membershipType.Type == "" || membershipType.Status == "" {
		return membershipType, errors.New("error membership type id not found")
	}

	if err != nil {
		return membershipType, err
	}

	return membershipType, nil
}
