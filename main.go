package main

import (
	"fmt"

	"github.com/gin-gonic/gin"
	"gitlab.com/merintisindonesia/membership-backend/routes"
)

/*
clean architecture :
- migration (auto migrate table and checking)
- model (ORM)
- repository (query, repo)
- service (business logic)
- handler (handle req res)
- middleware
- routes

package used:
- gin-gonic (http handle /REST API)
- GORM MYSQL (ORM) // prepare statement
- bcrypt (hashing pass) // salt 10 / default
- JWT (middleware)
- redis (cache ) // next phase
*/

func CORSMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Writer.Header().Set("Access-Control-Allow-Origin", "http://localhost:3000")
		c.Writer.Header().Set("Access-Control-Allow-Credentials", "true")
		c.Writer.Header().Set("Access-Control-Allow-Headers", "Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, accept, origin, Cache-Control, X-Requested-With")
		c.Writer.Header().Set("Access-Control-Allow-Methods", "POST, OPTIONS, GET, PUT, PATCH")

		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(204)
			return
		}

		c.Next()
	}
}

func main() {
	r := gin.Default()

	r.Static("/images", "./images")

	r.Use(CORSMiddleware())

	routes.MembershipTypeRoute(r)
	fmt.Println("")
	fmt.Println("===  MEMBERSHIP USER ROUTES API ===")
	fmt.Println("")

	routes.DataMembershipRoute(r)
	routes.MembershipContentRoute(r)
	routes.MembershipExamRoute(r)
	routes.CertificateRoute(r)
	routes.ContentRoute(r)
	routes.ExamRoute(r)
	routes.MembershipQuestExamRoute(r)

	fmt.Println("")
	fmt.Println("===  TALKSHOW ROUTES API ===")
	fmt.Println("")
	routes.TalkshowVideoRoute(r)
	fmt.Println("")
	fmt.Println("===  USER ROUTES API ===")
	fmt.Println("")
	routes.UserRoute(r)
	fmt.Println("")
	fmt.Println("===  REFFERAL ROUTES API ===")
	fmt.Println("")
	routes.RefferalRoute(r)

	r.Run()

	// TESTING ADD 1 YEAR TIME =========================================

	// dateNow := time.Now()
	// afterAYear := dateNow.AddDate(1, 0, 0)
	// fmt.Println(dateNow, afterAYear)

	// TESTING WHERE IN

	// result, _ := contentRepository.GetByMemberTypePremium("1", "2")

	// fmt.Println(result)
	// fmt.Println(len(result))

	// TESTING BYCRYP USING SALT 11 ====================================

	// pass := "pratama12"

	// hash, _ := bcrypt.GenerateFromPassword([]byte(pass), 11)

	// // result := fmt.Sprintf("%x", hash)

	// fmt.Println(string(hash))

	// hashPass := "$2y$11$Ru0OPgNlo4OV26o1wWTLQu6gZFLBQT7ig5vzQNQ/ou0DnIaSU5nFK"

	// err := bcrypt.CompareHashAndPassword([]byte(hashPass), []byte(pass))

	// fmt.Println(err)
}
